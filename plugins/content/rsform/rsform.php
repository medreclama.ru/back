<?php
/**
* @package RSForm! Pro
* @copyright (C) 2007-2015 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

defined('_JEXEC') or die('Restricted access');

class plgContentRSForm extends JPlugin
{
    // Добавляем в формы:
    // скрытое поле "req_name" - чтобы сюда спам-бот ввел данные
    // <script> - через JS будет добавлять системное поле (name="form[formId]")
    public function rsfp_f_onInitFormDisplay($args)
    {
        $botWillFillThisInput = '<input type="text" name="req_name" value="">';

        $hiddenFieldFormId = '<input type="hidden" name="form[formId]" value="'.$args['formId'].'"/>';
        $hiddenFieldFormId = "document.write('".$hiddenFieldFormId."')";
        $hiddenFieldFormId = '<script>'.$hiddenFieldFormId.'</script>';

        $args["formLayout"] = $botWillFillThisInput.$args["formLayout"]."\n".$hiddenFieldFormId;
    }

    // Отмена обработки формы при заполненом скрытом поле или наличии стоп-слов в тексте ее полей
    public function rsfp_f_onBeforeFormProcess($formFields)
    {
        $app = JFactory::getApplication();
        $errorMessage = "Неверные данные формы";

        // Если спам-робот ввел данные в поле которое обычный пользователь не видит
        if(!empty($_REQUEST["req_name"])){
            $app->enqueueMessage($errorMessage, "error");
            $this->saveRejectedSubmission($formFields);
            return false;
        }

        $blackListWords = [
            "loan",
            "credit",
            "cars",
            "cash",
            "insurance",
        ];

        // Проверка на наличие стоп-слов
        $formFields = $formFields["post"];
        foreach($formFields as $formFieldValue){
            foreach($blackListWords as $blackListWord){
                if(strpos($formFieldValue, $blackListWord) !== false){
                    $app->enqueueMessage($errorMessage, "error");
                    return false;
                }
            }
        }

        if(!empty($formFields["email"]) && $formFields["email"] == "01215@gmail.com"){
			$app->enqueueMessage($errorMessage, "error");
			return false;
		}

        $blackListPhones = [
        	"79207078647", "+7 (920) 707-86-47",
        	"79581000665", "+7 (958) 100-06-65",
        	"79675554799", "+7 (967) 555-47-99",
        	"79207017877", "+7 (920) 701-78-77",
        	"79310098278", "+7 (931) 009-82-78",
        	"79207017877", "+7 (920) 701-78-77",
        	"79509195788", "+7 (950) 919-57-88",
        	"79997753392", "+7 (999) 775-33-92",
		];
        if(!empty($formFields["phone"]) && in_array($formFields["phone"], $blackListPhones)){
			$app->enqueueMessage($errorMessage, "error");
			return false;
		}

        return true;
	}

    private function saveRejectedSubmission($saveData)
    {
        $logFileName = date("Y.m.d_H-i-s").".log";
        $savePath = $_SERVER["DOCUMENT_ROOT"]."/logs/submissionRejected/".$logFileName;

        $saveData["anti-spam_field-should_not_be_filled"] = $_REQUEST["name"];
        $saveData = "<? return [".var_export($saveData, true)."];";

        file_put_contents($savePath, $saveData);
	  }

	// Check if RSForm! Pro can be loaded
	protected function canRun() {
		if (class_exists('RSFormProHelper')) {
			return true;
		}
		
		$helper = JPATH_ADMINISTRATOR.'/components/com_rsform/helpers/rsform.php';
		if (file_exists($helper)) {
			require_once($helper);
			return true;
		}
		
		return false;
	}
	
	// Joomla! Triggers - onContentBeforeDisplay()
	public function onContentBeforeDisplay($context, &$row, &$params, $page=0) {
		// Don't run this plugin when the content is being indexed
		if ($context == 'com_finder.indexer') {
			return true;
		}
		
		if ($this->canRun()) {
			if (is_object($row) && isset($row->text)) {
				$this->_addForm($row->text);
			} elseif (is_string($row)) {
				$this->_addForm($row);
			}
		}
	}
	
	// Joomla! Triggers - onContentPrepare()
	public function onContentPrepare($context, &$row, &$params, $page=0) {
		// Don't run this plugin when the content is being indexed
		if ($context == 'com_finder.indexer') {
			return true;
		}

		if (is_object($row) && isset($row->text)) {
			$this->_addForm($row->text);
		} elseif (is_string($row)) {
			$this->_addForm($row);
		}
	}
	
	// Syntax replacement function
	protected function _addForm(&$text) {		
		// Performance check
		if (strpos($text, '{rsform ') !== false) {
			if (!$this->canRun()) {
				return false;
			}

			// Expression to search for
			$pattern = '#\{rsform (.*?)\}#i';
			// Found matches
			if (preg_match_all($pattern, $text, $matches)) {
				// No replacement when we're not dealing with HTML
				if (JFactory::getDocument()->getType() != 'html') {
					$text = preg_replace($pattern, '', $text);
					return true;
				}
				
				// Load language
				JFactory::getLanguage()->load('com_rsform', JPATH_SITE);
				
				// Disable caching
				$cache = JFactory::getCache('com_content');
				$cache->setCaching(false);
				
				foreach ($matches[0] as $i => $fullMatch) {
					if (preg_match_all('#[a-z0-9_]+=".*?"#i', $matches[1][$i], $attributesMatches)) {
						$data = array();
						
						foreach ($attributesMatches[0] as $pair) {
							list($attribute, $value) = explode('=', $pair, 2);
							
							$attribute  = html_entity_decode($attribute);
							$value 		= html_entity_decode(trim($value, '"'));
							
							if (isset($data[$attribute])) {
								if (!is_array($data[$attribute])) {
									$data[$attribute] = (array) $data[$attribute];
								}
								
								$data[$attribute][] = $value;
							} else {
								$data[$attribute] = $value;
							}
						}
						
						if ($data) {
							JFactory::getApplication()->input->get->set('form', $data);
						}
					}
					
					$pattern = '#\{rsform ([0-9]+)#i';
					if (preg_match($pattern, $fullMatch, $formMatch)) {
						$formId = $formMatch[1];
						$text 	= str_replace($fullMatch, RSFormProHelper::displayForm($formId, true), $text);
					}
				}
			}
		}
	}

}