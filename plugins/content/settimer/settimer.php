<? defined('_JEXEC') or die;

class PlgContentSettimer extends JPlugin
{
    const MODE_LAST_DAY_OF_MONTH = "getLastDayOfMonth";
    const MODE_FIRST_DAY_OF_MONTH = "getFirstDayOfMonth";

	/**
	 * @param   string   $context   The context of the content being passed to the plugin.
	 * @param   object   &$article  The article object.  Note $article->text is also available
	 * @param   mixed    &$params   The article params
	 * @param   integer  $page      The 'page' number
	 *
	 * @return  mixed   true if there is an error. Void otherwise.
	 *
	 * @since   1.6
	 */
	public function onContentPrepare($context, &$article, &$params, $page = 0)
	{
		if (strpos($article->text, 'set_timer') === false)
		{
			return true;
		}

		$text = $article->text;
		$regex = '/{set_timer (.*)}/iU';
		preg_match_all($regex, $text, $matches, PREG_SET_ORDER);

		if(!$matches){
		    return true;
		}

		foreach ($matches as $match){

		    if(!$match[1]){
		        continue;
		    }

		    $replacement = "";

            $pluginSignature = $match[0];
            $pluginConfigJson = $match[1];

            $pluginConfigObj = json_decode("{".$pluginConfigJson."}");

            if(!empty($pluginConfigObj->mode)){
                $mode = $pluginConfigObj->mode;
                $constName = "self::MODE_".$mode;

                $timerId = false;
                if(!empty($pluginConfigObj->timerId)){
                    $timerId = $pluginConfigObj->timerId;
                }

                if(defined($constName)){
                    $methodName = constant($constName);
                    if(method_exists($this, $methodName)){
                        $replacement = $this->$methodName($timerId);
                    }
                }

            }

            $text = str_replace($pluginSignature, $replacement, $text);
		}

		$article->text = $text;
	}

	private function getLastDayOfMonth($timerId)
	{
	    if(!$timerId){
	        $timerId = "countdown";
	    }

        $curMonthIndex = (int)date("m");
        $nextMonthIndex = ++$curMonthIndex;
        $curYear = (int)date("Y");

        $lastDayOfCurMonthTS = mktime(0, 0, 0, $nextMonthIndex, 0, $curYear);
        $lastDayOfCurMonthStr = date('d.m.Y', $lastDayOfCurMonthTS);

        $lastDayOfCurMonthStrJs = date('Y', $lastDayOfCurMonthTS);
        $lastDayOfCurMonthStrJs .= ', '.(date('m', $lastDayOfCurMonthTS) - 1);
        $lastDayOfCurMonthStrJs .= ', '.date('d', $lastDayOfCurMonthTS);
        $lastDayOfCurMonthStrJs .= ', 23, 59, 59';

        $dateObj = new JDate($lastDayOfCurMonthStr);
        $dateTxt = $dateObj->calendar("d F");

        $dateScript = '<script type="text/javascript">';
        $dateScript .= 'jQuery(function(){';
        $dateScript .= 'jQuery("#'.$timerId.'").countdown({timestamp: new Date('.$lastDayOfCurMonthStrJs.')});';
        $dateScript .= '});';
        $dateScript .= '</script>';

        return $dateTxt.$dateScript;
	}

	private function getFirstDayOfMonth(){}

}
