<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

$rep_id = (int) $rep_id;

$error = '';
if(isset($_POST['document']) AND $_POST['document']){
    $document_name  = 'getreputation/'.(int) $_POST['document'];
    $document_name .= '.doc';

    if(file_exists($document_name)){
        header("location: ".$document_name);
    }else{
        echo "<h3 style='color:red; padding: 5px 0;'>Документ не найден</h3>";
    }

}
?>
<h3>Для того чтобы получить аудит репутации, пожалуйста, нажмите кнопку "Открыть"</h3>
<br>
<form method="post">
	<input type="submit" name="submit" value="Открыть" class="km_submit">
	<input type="hidden" name="document" value="<?php echo $rep_id; ?>">
</form>
