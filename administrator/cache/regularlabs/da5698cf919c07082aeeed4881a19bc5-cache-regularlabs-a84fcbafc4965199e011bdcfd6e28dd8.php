<?php die("Access Denied"); ?>#x#s:1230:"<?xml version="1.0" encoding="utf-8"?>
<extensions>
	<extension>
		<name><![CDATA[Advanced Module Manager]]></name>
		<id>advancedmodulemanager</id>
		<alias>advancedmodulemanager</alias>
		<extname>advancedmodules</extname>
		<version>9.9.0</version>
		<has_pro>1</has_pro>
		<pro>0</pro>
		<downloadurl>https://download.regularlabs.com/file.php?ext=advancedmodulemanager&amp;j=3</downloadurl>
		<downloadurl_pro></downloadurl_pro>
		<changelog><![CDATA[
<div style="font-family:monospace;font-size:1.2em;">15-Jan-2024 : v10.0.18<br> # Fixes issue with installation error about table Array (again)<br><br>28-Dec-2023 : v10.0.17<br> # Fixes issue with installation error about table Array<br><br>28-Dec-2023 : v10.0.16<br> # [PRO] Fixes issue with php error about getAgent returning null<br><br>20-Dec-2023 : v10.0.15<br> # Fixes issue with assignments not being converted to conditions on upgrade from J3 to J4<br> # Fixes issue with php error about selectList on select fields with loads of items<br><br>13-Dec-2023 : v10.0.14<br> # Fixes issue with breaking error about passSimple when using certain conditions</div><div style="text-align: right;"><i>...click for more...</i></div>
		]]></changelog>
	</extension>
</extensions>";