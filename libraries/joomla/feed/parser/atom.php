<?php
/**
 * @package     Joomla.Platform
 * @subpackage  Feed
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

/**
 * ATOM Feed Parser class.
 *
 * @link   http://www.atomenabled.org/developers/syndication/
 * @since  12.3
 */
class JFeedParserAtom extends JFeedParser
{
	/**
	 * @var    string  The feed format version.
	 * @since  12.3
	 */
	protected $version;

	/**
	 * Method to handle the `<author>` element for the feed.
	 *
	 * @param   JFeed             $feed  The JFeed object being built from the parsed feed.
	 * @param   SimpleXMLElement  $el    The current XML element object to handle.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function handleAuthor(JFeed $feed, SimpleXMLElement $el)
	{
		// Set the author information from the XML element.
		$feed->setAuthor(
			$this->inputFilter->clean((string) $el->name, 'html'),
			filter_var((string) $el->email, FILTER_VALIDATE_EMAIL),
			filter_var((string) $el->uri, FILTER_VALIDATE_URL)
		);
	}

	/**
	 * Method to handle the `<contributor>` element for the feed.
	 *
	 * @param   JFeed             $feed  The JFeed object being built from the parsed feed.
	 * @param   SimpleXMLElement  $el    The current XML element object to handle.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function handleContributor(JFeed $feed, SimpleXMLElement $el)
	{
		$feed->addContributor(
			$this->inputFilter->clean((string) $el->name, 'html'),
			filter_var((string) $el->email, FILTER_VALIDATE_EMAIL),
			filter_var((string) $el->uri, FILTER_VALIDATE_URL)
		);
	}

	/**
	 * Method to handle the `<generator>` element for the feed.
	 *
	 * @param   JFeed             $feed  The JFeed object being built from the parsed feed.
	 * @param   SimpleXMLElement  $el    The current XML element object to handle.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function handleGenerator(JFeed $feed, SimpleXMLElement $el)
	{
		$feed->generator = $this->inputFilter->clean((string) $el, 'html');
	}

	/**
	 * Method to handle the `<id>` element for the feed.
	 *
	 * @param   JFeed             $feed  The JFeed object being built from the parsed feed.
	 * @param   SimpleXMLElement  $el    The current XML element object to handle.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function handleId(JFeed $feed, SimpleXMLElement $el)
	{
		$feed->uri = (string) $el;
	}

	/**
	 * Method to handle the `<link>` element for the feed.
	 *
	 * @param   JFeed             $feed  The JFeed object being built from the parsed feed.
	 * @param   SimpleXMLElement  $el    The current XML element object to handle.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function handleLink(JFeed $feed, SimpleXMLElement $el)
	{
		$link = new JFeedLink;
		$link->uri      = (string) $el['href'];
		$link->language = (string) $el['hreflang'];
		$link->length   = (int) $el['length'];
		$link->relation = (string) $el['rel'];
		$link->title    = $this->inputFilter->clean((string) $el['title'], 'html');
		$link->type     = (string) $el['type'];

		$feed->link = $link;
	}

	/**
	 * Method to handle the `<rights>` element for the feed.
	 *
	 * @param   JFeed             $feed  The JFeed object being built from the parsed feed.
	 * @param   SimpleXMLElement  $el    The current XML element object to handle.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function handleRights(JFeed $feed, SimpleXMLElement $el)
	{
		$feed->copyright = $this->inputFilter->clean((string) $el, 'html');
	}

	/**
	 * Method to handle the `<subtitle>` element for the feed.
	 *
	 * @param   JFeed             $feed  The JFeed object being built from the parsed feed.
	 * @param   SimpleXMLElement  $el    The current XML element object to handle.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function handleSubtitle(JFeed $feed, SimpleXMLElement $el)
	{
		$feed->description = $this->inputFilter->clean((string) $el, 'html');
	}

	/**
	 * Method to handle the `<title>` element for the feed.
	 *
	 * @param   JFeed             $feed  The JFeed object being built from the parsed feed.
	 * @param   SimpleXMLElement  $el    The current XML element object to handle.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function handleTitle(JFeed $feed, SimpleXMLElement $el)
	{
		$feed->title = $this->inputFilter->clean((string) $el, 'html');
	}

	/**
	 * Method to handle the `<updated>` element for the feed.
	 *
	 * @param   JFeed             $feed  The JFeed object being built from the parsed feed.
	 * @param   SimpleXMLElement  $el    The current XML element object to handle.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function handleUpdated(JFeed $feed, SimpleXMLElement $el)
	{
		$feed->updatedDate = $this->inputFilter->clean((string) $el, 'html');
	}

	/**
	 * Method to initialise the feed for parsing.  Here we detect the version and advance the stream
	 * reader so that it is ready to parse feed elements.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function initialise()
	{
		// Read the version attribute.
		$this->version = ($this->stream->getAttribute('version') == '0.3') ? '0.3' : '1.0';

		// We want to move forward to the first element after the root element.
		$this->moveToNextElement();
	}

	/**
	 * Method to handle a `<entry>` element for the feed.
	 *
	 * @param   JFeedEntry        $entry  The JFeedEntry object being built from the parsed feed entry.
	 * @param   SimpleXMLElement  $el     The current XML element object to handle.
	 *
	 * @return  void
	 *
	 * @since   12.3
	 */
	protected function processFeedEntry(JFeedEntry $entry, SimpleXMLElement $el)
	{
		$entry->uri         = (string) $el->id;
		$entry->title       = $this->inputFilter->clean((string) $el->title, 'html');
		$entry->updatedDate = $this->inputFilter->clean((string) $el->updated, 'html');
		$entry->content     = $this->inputFilter->clean((string) $el->summary, 'html');

		if (!$entry->content)
		{
			$entry->content = $this->inputFilter->clean((string) $el->content, 'html');
		}

		if (filter_var($entry->uri, FILTER_VALIDATE_URL) === false && !is_null($el->link) && $el->link)
		{
			$link = $el->link;
			if (is_array($link))
			{
				$link = $this->bestLinkForUri($link);
			}
			$uri = (string) $link['href'];
			if ($uri)
			{
				$entry->uri = $uri;
			}
		}
	}

	/**
	 * If there is more than one <link> in the feed entry, find the most appropriate one and return it.
	 *
	 * @param   array  $links  Array of <link> elements from the feed entry.
	 *
	 * @return  SimpleXMLElement
	 */
	private function bestLinkForUri(array $links)
	{
		$linkPrefs = array('', 'self', 'alternate');
		foreach ($linkPrefs as $pref)
		{
			foreach ($links as $link)
			{
				$rel = (string) $link['rel'];
				if ($rel === $pref)
				{
					return $link;
				}
			}
		}
		return array_shift($links);
	}
}
