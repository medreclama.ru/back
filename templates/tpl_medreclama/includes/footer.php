<footer class="footer-main">
  <div class="container">
    <div class="content">
      <div class="grid">
        <jdoc:include type="modules" name="footer" style="simple" />
        <div class="grid__cell grid__cell--m-12 grid__cell--12 footer-main__bottom">
          <div class="footer-copy">© 2008-<?= date("Y") ?> "Medreclama" - медицинский маркетинг</div>
          <div class="footer-main__agreements"><a href="/soglasheniya-na-obrabotku-personalnykh-dannykh">Соглашения на обработку персональных данных</a> | <a href="/pravila-ispolzovaniya-sayta">Правила использования сайта</a></div>
        </div>
      </div>
    </div>
  </div>
  <? include __DIR__."/footer/popups.php" ?>
  <? include __DIR__."/footer/debug.php" ?>
</footer>
