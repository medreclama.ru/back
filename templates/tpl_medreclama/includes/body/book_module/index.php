<? if($isMobileDetected == false): ?>
	<div class="book-module" id="book-module">
		<div class="book-module__close"></div>
		<div class="book-module__inner">
			<div class="book-module__title">
				Скачать бесплатно книгу в pdf <strong>"Конкурентные преимущества в рекламе клиники"</strong>
			</div>

			<?/* <? include __DIR__."/form.php" ?> */?>

			<p class="book-module__info"><strong>Благодаря книге</strong> вы сможете сформулировать собственное позиционирование и <strong>увеличить конверсию</strong> сайта клиники минимум <strong>на 30%</strong> без дополнительных вложений.</p>
		</div>
		<div class="book-module__open"><span>Книга в подарок</span></div>
	</div>
<? else: ?>
	<div class="book-module-mobile" id="book-module-mobile">
		<div class="book-module-mobile__close"></div>
		<div class="book-module-mobile__inner">
			<div class="book-module-mobile__title">
				Скачать бесплатно книгу в pdf <strong>"Конкурентные преимущества в рекламе клиники"</strong>
			</div>

            <?/* <? include __DIR__."/form.php" ?> */?>
            
			<p class="book-module-mobile__info"><strong>Благодаря книге</strong> вы сможете сформулировать собственное позиционирование и <strong>увеличить конверсию</strong> сайта клиники минимум <strong>на 30%</strong> без дополнительных вложений.</p>
		</div>
		<div class="book-module-mobile__open"><span>Книга в подарок</span></div>
	</div>
<? endif ?>
