<main>
  <div class="container">
    <div class="content content--white <?= $pageclass ?>">

      <? if($curUrl == "/"): ?>

        <? include __DIR__."/home/results.php" ?>
        <? include __DIR__."/home/contact_us.php" ?>
        <? include __DIR__."/home/why_we.php" ?>
        <? include __DIR__."/home/directions.php" ?>
        <? include __DIR__."/home/about_me.php" ?>
        <? include __DIR__."/home/process.php" ?>
        <? include __DIR__."/home/cases.php" ?>
        <? include __DIR__."/home/feedback.php" ?>
        <? include __DIR__."/home/audits.php" ?>
        <? include __DIR__."/home/social.php" ?>
        <? include __DIR__."/home/partners.php" ?>
        <? include __DIR__."/home/request.php" ?>

      <? else: ?>

		<? if ($this->countModules('right_column')): ?>
        <div class="grid grid--padding-y ">
            <div class="grid__cell grid__cell--m-8 grid__cell--12 ">
        <? endif ?>

        <? if ($this->countModules('content_before')): ?>
          <div class="content_before">
            <jdoc:include type="modules" name="content_before" style="raw" />
          </div>
        <? endif ?>

        <div class="content_component">
          <jdoc:include type="message" />
          <jdoc:include type="component" />
        </div>

        <? if ($this->countModules('content_under')): ?>
          <div class="content_under">
            <jdoc:include type="modules" name="content_under" style="raw" />
          </div>
        <? endif ?>

        <? if ($this->countModules('right_column')): ?>
                </div> <!-- grid__cell grid__cell--m-8 grid__cell--12 -->

                <div class="grid__cell grid__cell--m-4 grid__cell--12 ">
                    <div class="grid grid--padding-y ">
                        <jdoc:include type="modules" name="right_column" style="raw" />
                    </div>
                </div>

            </div> <!-- grid grid--padding-y -->
        <? endif ?>


      <? endif ?>

    </div>
  </div>
</main>
