<meta charset="utf-8">
<?
$curUrl = str_replace("?".$_SERVER["QUERY_STRING"], "", $_SERVER["REQUEST_URI"]);

$template	= $this->template;

$menus = JFactory::getApplication("site")->getMenu();

$menuActive = $menus->getActive();
$menuActiveId = $menuActive->id;

$menuDefault = $menus->getDefault();
$menuDefaultId = $menuDefault->id;

$pageclass = "";

if (is_object($menuActive)) :
  $pageclass_sfx = $menuActive->params->get('pageclass_sfx');
  $pageclass = ($pageclass_sfx) ? ' '.$pageclass_sfx : '';
endif;

if($_SERVER['REMOTE_ADDR'] != "127.0.0.1") {
  $GLOBALS['prod'] = true;
}

$numb_page = "all";

if($menuActiveId == $menuDefaultId){
  $numb_page = "first";
}

$left = ($this->countModules('left')) ? '-left' : '';
$right = ($this->countModules('right')) ? '-right' : '';

$curComponent = JFactory::getApplication()->input->get('option');
$curController = JFactory::getApplication()->input->get('controller');

unset(
  $this->_scripts[$this->baseurl.'/media/system/js/mootools-core.js'],
  $this->_scripts[$this->baseurl.'/media/system/js/mootools-more.js'],
  $this->_scripts[$this->baseurl.'/media/system/js/core.js'],
  $this->_scripts[$this->baseurl.'/media/system/js/caption.js']
);
?>
<script src="/templates/<?= $template ?>/scripts/jquery-3.3.1.min.js"></script>

<jdoc:include type="head" />

<?php if(JURI::current()!= JURI::base()){
    $document = JFactory::getDocument();
    $string = $document->getTitle();
    $pattern = '/\/.*/';
    $result = preg_replace($pattern, '', $string);
    $document->setTitle($result);

} ?>


<? include $_SERVER['DOCUMENT_ROOT'].'/includes/Mobile_Detect.php'; ?>
<? $mobileDetect = new Mobile_Detect; ?>
<? $isMobileDetected = $mobileDetect->isMobile(); ?>

<?php
$document = JFactory::getDocument();
$document->addStyleSheet('templates/system/css/system.css');
$document->addStyleSheet('templates/'.$template.'/styles/joomla-core.css');
$document->addStyleSheet('templates/'.$template.'/styles/jquery.fancybox.min.css');
$document->addStyleSheet('/local/front/template/styles/style.css?v=5');
$document->addStyleSheet('templates/'.$template.'/styles/add.css?v=4');
?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="favicon.png" />
<? include __DIR__."/head/before_head_closing.php" ?>
