<div id="audit_kompleksnyi" style="display: none; width: 400px;">
  <form
    class="form audit_kompleksnyi validated-form submitJS"
    method="post"
    action="/index.php?option=com_rsform&view=rsform&formId=24"
  >

      <div class="rsform_title form__header">Комплексный аудит интернет-маркетинга клиники</div>

      <input type="text" name="req_name" value="">

      <div class="rsform_row form__row">
        <input type="text" name="form[site]" title="" class="form-input validated required" placeholder="Ваш сайт">
        <div class="validation-msg validation-msg-required"></div>
      </div>

      <div class="rsform_row form__row">
        <input type="text" name="form[fio]" title="" class="form-input validated required regex" data-regex="[а-яё]" placeholder="Ваше имя">
        <div class="validation-msg validation-msg-required"></div>
        <div class="validation-msg validation-msg-regex">Должны быть русские буквы</div>
      </div>

      <div class="rsform_row form__row">
        <input type="tel" name="form[phone]" title="" class="form-input rsform_phone validated required" placeholder="Телефон">
        <div class="validation-msg validation-msg-required"></div>
      </div>

      <div class="rsform_row form__row">
        <input type="text" name="form[email]" title="" class="form-input validated required" placeholder="Электронная почта">
        <div class="validation-msg validation-msg-required"></div>
      </div>

      <div class="rsform_row form__submit form__row form__row--small-margin">
        <button type="submit" name="form[send]" class="rsform-submit-button btn">Заказать аудит</button>
        <div class="validation-msg validation-msg-required"></div>
      </div>
      <div class="text-marked text-marked--size--xs">
        Оставляя заявку, вы соглашаетесь на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh">обработку персональных данных</a>
      </div>

      <script>document.write('<input type="hidden" name="form[formId]" value="24"/>')</script>

  </form>
</div>
