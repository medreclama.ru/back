<div class="case-offer__modal" id="case-offer__modal" style="display: none; width: 400px;">
   <form
      class="form case-offer__form validated-form submitJS"
      id="case-offer-form"
      method="post"
      action="/index.php?option=com_rsform&view=rsform&formId=17"
    >

      <div class="form__header">Прогноз бюджета</div>
      <div class="form__row">Мы бесплатно проанализируем Ваш сайт и вышлем Вам развернутое предложение:</div>

      <input type="text" name="req_name" value="">

      <div class="form__row">
         <input
          class="form-input validated required"
          type="text" name="form[site]"
          placeholder="Ваш сайт"
        >
        <div class="validation-msg validation-msg-required"></div>
      </div>

      <div class="form__row">
         <input
          class="form-input validated required"
          type="text"
          name="form[fio]"
          placeholder="Ваше имя"
        >
        <div class="validation-msg validation-msg-required"></div>
      </div>

      <div class="form__row">
         <input
          class="form-input validated required"
          type="tel"
          name="form[phone]"
          placeholder="+7 ( ) - - "
        >
        <div class="validation-msg validation-msg-required"></div>
      </div>

      <div class="form__row">
         <input
          class="form-input validated required"
          type="email" name="form[email]"
          placeholder="Электронная почта"
        >
        <div class="validation-msg validation-msg-required"></div>
      </div>

      <div class="form__row">
         <textarea
          class="form-input validated required"
          name="form[message]"
          placeholder="Вид услуг, ваши пожелания"
        ></textarea>
        <div class="validation-msg validation-msg-required"></div>
      </div>

      <div class="form__submit">
         <button class="btn" type="submit">Заказать расчёт</button>
      </div>

      <script>document.write('<input type="hidden" name="form[formId]" value="17"/>')</script>

   </form>
</div>

