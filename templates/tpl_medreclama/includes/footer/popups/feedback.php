<div id="feedback_form" style="display: none; width: 400px;">
  <form
    class="form feedback validated-form submitJS"
    method="post"
    action="/index.php?option=com_rsform&view=rsform&formId=14"
  >

    <div class="form__header">Написать нам</div>

    <input type="text" name="req_name" value="">

    <div class="form__row">
      <div class="form-caption">Имя</div>
      <input name="form[fio]" class="form-input validated required regex" data-regex="[а-яё]" type="text">
      <div class="validation-msg validation-msg-required"></div>
      <div class="validation-msg validation-msg-regex">Должны быть русские буквы</div>
    </div>

    <div class="form__row">
      <div class="form-caption">E-mail</div>
      <input name="form[email]" class="form-input validated required" type="text">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="form__row">
      <div class="form-caption">Текст сообщения</div>
      <textarea cols="50" rows="5" name="form[message]" class="form-input validated required"></textarea>
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="form__submit">
      <input name="form[submit]" class="btn rsform-submit-button" value="Отправить заявку" type="submit">
    </div>

    <script>document.write('<input type="hidden" name="form[formId]" value="14"/>')</script>

  </form>
</div>
