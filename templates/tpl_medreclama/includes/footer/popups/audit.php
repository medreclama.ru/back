<div id="audit_form" style="display: none; width: 400px;">
  <form
    class="form audit_offer validated-form submitJS"
    method="post"
    action="/index.php?option=com_rsform&view=rsform&formId=25"
  >

    <div class="rsform_title form__header">Заказать аудит</div>

    <input type="text" name="req_name" value="">

    <div class="rsform_row form__row">
      <input name="form[site]" title="" class="form-input validated required" placeholder="Ваш сайт" type="text">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="rsform_row form__row">
      <input name="form[fio]" title="" class="form-input validated required regex" data-regex="[а-яё]" placeholder="Ваше имя" type="text">
      <div class="validation-msg validation-msg-required"></div>
      <div class="validation-msg validation-msg-regex">Должны быть русские буквы</div>
    </div>

    <div class="rsform_row form__row">
      <input name="form[phone]" title="" class="form-input validated required" placeholder="Ваш телефон" type="tel">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="rsform_row form__row">
      <input name="form[email]" title="" class="form-input validated required" placeholder="Электронная почта" type="text">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="radio_block ">
      <div class="rsform_row validated-type-radio">
        <div class="home-form__input home-form__input--radio">
          <input name="form[service]" value="Комплексный аудит интернет-маркетинга" id="service0" class="validated required" type="radio">
          <label for="service0">Комплексный аудит интернет-маркетинга</label><br>
        </div>
        <div class="home-form__input home-form__input--radio">
          <input name="form[service]" value="Аудит продающих качеств сайта" id="service1" class="validated required" type="radio">
          <label for="service1">Аудит продающих качеств сайта</label>
        </div>
        <div class="home-form__input home-form__input--radio">
          <input name="form[service]" value="Аудит воронки продаж клиники" id="service2" class="validated required" type="radio">
          <label for="service2">Аудит воронки продаж клиники</label>
        </div>
        
        <? /* <span id="component116" class="formNoError">Неверный ввод</span> */ ?>
      </div>
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="rsform_row form__submit">
      <input name="form[send]" class="rsform-submit-button btn" value="Заказать аудит" type="submit">
    </div>

    <script>document.write('<input type="hidden" name="form[formId]" value="25"/>')</script>

  </form>
</div>
