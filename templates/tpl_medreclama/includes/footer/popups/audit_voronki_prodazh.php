<div id="audit_voronki_prodazh" style="display: none; width: 400px;">
  <form
    class="form audit_prodayuschih_kachestv validated-form submitJS"
    method="post"
    action="/index.php?option=com_rsform&view=rsform&formId=26"
  >

    <div class="form__header">Аудит воронки продаж клиники</div>

    <input type="text" name="req_name" value="">

    <div class="rsform_row form__row">
      <input type="text" name="form[site]" class="form-input validated required" placeholder="Ваш сайт">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="rsform_row form__row">
      <input type="text" name="form[fio]" class="form-input validated required regex" data-regex="[а-яё]" placeholder="Ваше имя">
      <div class="validation-msg validation-msg-required"></div>
      <div class="validation-msg validation-msg-regex">Должны быть русские буквы</div>
    </div>

    <div class="rsform_row form__row">
      <input type="tel" name="form[phone]" class="form-input rsform_phone validated required" placeholder="Телефон">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="rsform_row form__row">
      <input type="text" name="form[email]" class="form-input validated required" placeholder="Электронная почта">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="rsform_row form__submit form__row form__row--small-margin">
      <button type="submit" name="form[send]" class="btn rsform-submit-button">Заказать аудит</button>
    </div>
    <div class="text-marked text-marked--size--xs">
      Оставляя заявку, вы соглашаетесь на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh">обработку персональных данных</a>
    </div>
    <script>document.write('<input type="hidden" name="form[formId]" value="26"/>')</script>

  </form>
</div>
