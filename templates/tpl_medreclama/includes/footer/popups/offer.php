<div id="offer_form" style="display: none;">
  <h2>Получить коммерческое предложение</h2>
  <div class="home-form">
    <form
      class="home-form__form home-form__request validated-form submitJS"
      method="post"
      action="/index.php?option=com_rsform&view=rsform&formId=21">

      <div class="grid grid--padding-y grid--justify-space-between">
        <div class="grid__cell grid__cell--m-7 grid__cell--12">

          <input type="text" name="req_name" value="">

          <div class="home-form__input">
            <label class="home-form__label" for="popup__task">Краткое описание задачи</label>
            <textarea class="form-input" id="popup__task" name="form[task]"> </textarea>
          </div>

          <div class="grid">
            <div class="grid__cell grid__cell--m-6 grid__cell--12">
              <div class="home-form__input">
                <label class="home-form__label home-form__label--required" for="popup__name">Имя</label>
                <input class="form-input validated required regex" data-regex="[а-яё]" id="popup__name" name="form[fio]">
                <div class="validation-msg validation-msg-required"></div>
                <div class="validation-msg validation-msg-regex">Должны быть русские буквы</div>
              </div>

              <div class="home-form__input">
                <label class="home-form__label home-form__label--required" for="popup__email">Электронная почта</label>
                <input class="form-input validated required" id="popup__email" name="form[email]">
                <div class="validation-msg validation-msg-required"></div>
              </div>
            </div>
            
            <div class="grid__cell grid__cell--m-6 grid__cell--12">
              <div class="home-form__input">
                <label class="home-form__label home-form__label--required" for="popup__tel">Телефон</label>
                <input class="form-input validated required" id="popup__tel" type="tel" name="form[phone]">
                <div class="validation-msg validation-msg-required"></div>
              </div>

              <div class="home-form__input">
                <label class="home-form__label home-form__label--required" for="popup__site">Ваш сайт</label>
                <input class="form-input validated required" id="popup__site" name="form[site]">
                <div class="validation-msg validation-msg-required"></div>
              </div>
            </div>
          </div>

        </div>

        <div class="grid__cell grid__cell--m-5 grid__cell--12 home-form__info">
          <span>
            <strong>Заря Виталий</strong><p class="text-marked text-marked--size--xs">Руководитель агентства</p>
          </span>
          <a href="mailto:sale@medreclama.ru">sale@medreclama.ru </a>
          <div class="text-marked text-marked--size--xs">Ответим в течение одного рабочего дня</div>
          <div class="home-form__submit">
            <button class="btn btn--primary" type="submit">Отправить заявку</button>
          </div>
          <div class="text-marked text-marked--size--xs">
            Оставляя заявку, вы соглашаетесь на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh">обработку персональных данных</a>
          </div>
        </div>
          
        <script>document.write('<input type="hidden" name="form[formId]" value="21"/>')</script>
        
      </div>
    </form>

  </div>
</div>
