<div id="advertisement_form" style="display: none; width: 400px;">
  <form
    class="form audit_offer validated-form submitJS"
    method="post"
    action="/index.php?option=com_rsform&view=rsform&formId=27"
  >

    <div class="rsform_title form__header">Заявка на рекламу</div>

    <input type="text" name="req_name" value="">

    <div class="rsform_row form__row">
      <input name="form[site]" title="" class="form-input validated required" placeholder="Ваш сайт" type="text">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="rsform_row form__row">
      <input name="form[fio]" title="" class="form-input validated required regex" data-regex="[а-яё]" placeholder="Ваше имя" type="text">
      <div class="validation-msg validation-msg-required"></div>
      <div class="validation-msg validation-msg-regex">Должны быть русские буквы</div>
    </div>

    <div class="rsform_row form__row">
      <input name="form[phone]" title="" class="form-input validated required" placeholder="Ваш телефон" type="tel">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="rsform_row form__row">
      <input name="form[email]" title="" class="form-input validated required" placeholder="Электронная почта" type="text">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="radio_block ">
      <div class="rsform_row validated-type-radio">
        <div class="home-form__input home-form__input--radio">
          <input name="form[service]" value="Поисковая оптимизация" id="service0" class="validated required" type="radio">
          <label for="service0">Поисковая оптимизация</label><br>
        </div>
        <div class="home-form__input home-form__input--radio">
          <input name="form[service]" value="Контекстная реклама" id="service1" class="validated required" type="radio">
          <label for="service1">Контекстная реклама</label>
        </div>
        <div class="home-form__input home-form__input--radio">
          <input name="form[service]" value="Маркетинг в социальных сетях" id="service2" class="validated required" type="radio">
          <label for="service2">Маркетинг в социальных сетях</label>
        </div>
        
        <? /* <span id="component116" class="formNoError">Неверный ввод</span> */ ?>
      </div>
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="rsform_row form__submit">
      <input name="form[send]" class="rsform-submit-button btn" value="Отправить" type="submit">
    </div>

    <script>document.write('<input type="hidden" name="form[formId]" value="27"/>')</script>

  </form>
</div>
