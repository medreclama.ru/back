<div id="konsultaciya_po_privlecheniyu_pacientov" style="display: none; width: 400px;">
  <form
    class="form konsultaciya_po_privlecheniyu_pacientov validated-form submitJS"
    method="post"
    action="/index.php?option=com_rsform&view=rsform&formId=22"
  >

    <div class="rsform_title form__header">Консультация по привлечению пациентов в клинику</div>

    <input type="text" name="req_name" value="">

    <div class="form__row rsform_row">
      <input type="text" name="form[site]" class="form-input validated required" placeholder="Ваш сайт">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="form__row rsform_row">
      <input type="text" name="form[fio]" class="form-input validated required regex" data-regex="[а-яё]" placeholder="Ваше имя">
      <div class="validation-msg validation-msg-required"></div>
      <div class="validation-msg validation-msg-regex">Должны быть русские буквы</div>
    </div>

    <div class="form__row rsform_row">
      <input type="tel" name="form[phone]" class="form-input rsform_phone validated required" placeholder="Телефон">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="form__row rsform_row">
      <input type="text" name="form[email]" class="form-input validated required" placeholder="Электронная почта">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="form__submit rsform_row form__row form__row--small-margin">
      <input type="submit" name="form[send]" class="btn rsform-submit-button" value="Заказать расчет">
    </div>
    <div class="text-marked text-marked--size--xs">
      Оставляя заявку, вы соглашаетесь на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh">обработку персональных данных</a>
    </div>
    <script>document.write('<input type="hidden" name="form[formId]" value="22"/>')</script>

  </form>
</div>
