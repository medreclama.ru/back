<div id="callback_form" style="display: none; width: 400px;">
  <form
    method="post"
    class="form call_back validated-form submitJS"
    action="/index.php?option=com_rsform&view=rsform&formId=2"
  >

    <div class="form__header">Запросить обратный звонок</div>

    <input type="text" name="req_name" value="">
    
    <div class="form__row">
      <label class="form__label--required form__label" for="zaprosit-zvonok-name">Имя</label>
      <input class="form-input rsform-input-box validated required regex" data-regex="[а-яё]" name="form[name]" id="zaprosit-zvonok-name" placeholder="Имя" type="text">
      <div class="validation-msg validation-msg-required"></div>
      <div class="validation-msg validation-msg-regex">Должны быть русские буквы</div>
    </div>

    <div class="form__row">
      <label class="form__label--required form__label" for="zaprosit-zvonok-tel">Телефон</label>
      <input class="form-input rsform-input-box validated required" name="form[phone]" id="zaprosit-zvonok-tel" placeholder="Телефон" type="tel">
      <div class="validation-msg validation-msg-required"></div>
    </div>

    <div class="form__submit form__row form__row--small-margin">
      <button class="btn" type="submit">Отправить</button>
    </div>
    <div class="text-marked text-marked--size--xs">
      Оставляя заявку, вы соглашаетесь на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh">обработку персональных данных</a>
    </div>

    <script>document.write('<input type="hidden" name="form[formId]" value="2"/>')</script>

  </form>

</div>
