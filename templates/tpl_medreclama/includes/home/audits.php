<section class="home-section"> 
    <h2>Проводим аудиты маркетинга клиники           </h2>
    <div class="grid grid--padding-y grid--justify-center grid--align-center  home-reasons page-subsection">
    <div class="grid__cell grid__cell--m-4 grid__cell--s-3 grid__cell--12 text-align-center">
        <p>Выполнили <br> 
        <span class="text-marked text-marked--size--m text-marked--font-weight--600 text-marked--color-purple">100+
        </span> аудитов
        </p>
    </div>
    <div class="grid__cell grid__cell--m-8 grid__cell--s-9 grid__cell--12 ">
        <h3 class="home-reasons__title">3 причины заказать аудит</h3>
        <ul class="list-marked">
        <li>Вы не понимаете за что платите деньги?</li>
        <li>Вы хотите проверить работу ваших подрядчиков?</li>
        <li>Несмотря на все усилия, реклама не привлекает пациентов?</li>
        </ul>
    </div>
    </div>

    <div class="grid grid--padding-y page-subsection">
            <div class="grid__cell grid__cell--m-7 grid__cell--s-12 grid__cell--12">
              <div class="youtube-responsive">
                <div class="youtube-responsive__iframe" data-src='9JMrSPFIQNw'>
                  <iframe frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
            </div>
            <div class="grid__cell grid__cell--m-5 grid__cell--s-12 grid__cell--12">
              <p><img src="/images/feedback/svetofor.svg" loading="lazy" alt="Стоматологическая клиника Светофор" /></p>
              <p>Виталий дал ряд практических рекомендаций по продвижению клиники в интернете. На сегодняшний день мы уже внедрили часть рекомендации и я доволен полученным результатом.</p>
              <p style="font-size: .875rem; font-style: italic; line-height: 1.5; text-align: right; color: #999;">
                Казаков Виктор<br>
                руководитель клиники
              </p>
            </div>
          </div>
</section>
