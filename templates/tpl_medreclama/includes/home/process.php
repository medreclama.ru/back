<section class="home-section">
  <h2>Как мы достигаем результатов?</h2>
  <div class="home-process">
    <ul class="home-process__list">
      <li class="home-process__item">
        <h3 class="home-process__title">Анализируем всю воронку продаж клиники
          <svg class="home-process__arrow" width="584" height="345" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
              <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
              <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
            </path>
          </svg>
        </h3>
        <div class="home-process__body">
          <div class="grid  list-analyze">
            <div class="grid__cell grid__cell--m-4 list-analyze__item">
              <div class="list-analyze__title">Реклама</div>
              <div class="list-analyze__body">
                <ul class="list-marked">
                  <li>Охват и целевая аудитория</li>
                  <li>Каналы рекламы</li>
                  <li>Количество переходов</li>
                  <li>Качество трафика</li>
                  <li>Стоимость перехода</li>
                </ul>
              </div>
            </div>
            <div class="grid__cell grid__cell--m-4 list-analyze__item">
              <div class="list-analyze__title">Сайт</div>
              <div class="list-analyze__body">
                <ul class="list-marked">
                  <li>Количество заявок</li>
                  <li>Конверсия сайта</li>
                  <li>Стоимость заявки</li>
                </ul>
              </div>
            </div>
            <div class="grid__cell grid__cell--m-4 list-analyze__item">
              <div class="list-analyze__title">Первичные пациенты</div>
              <div class="list-analyze__body">
                <ul class="list-marked">
                  <li>Количество первичных пациентов</li>
                  <li>Конверсия администраторов</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </li>
      <li class="home-process__item">
        <h3 class="home-process__title">Налаживаем отчетность
          <svg class="home-process__arrow" width="584" height="345" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
              <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
              <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
            </path>
          </svg>
        </h3>
        <div class="home-process__body">
          <div class="grid grid--align-center ">
            <div class="grid__cell grid__cell--m-7 grid__cell--s-12 grid__cell--12 ">
              <h4 class="home-process__subtitle">Организуем учет заявок от пациентов</h4>
              <ul class="list-marked home-process__list-marked">
                <li>Настраиваем calltracking (подмену номеров) и цели в веб аналитике</li>
              </ul>
              <h4 class="home-process__subtitle">Оцениваем эффективность рекламы</h4>
              <ul class="list-marked home-process__list-marked">
                <li>Анализируем эффективность различных каналов интернет рекламы</li>
                <li>Рассчитываем стоимость заявки (CPL), привлечения пациента (CPO), ДРР</li>
                <li>Принимаем решение о перераспределении бюджетов в эффективные каналы</li>
              </ul>
            </div>
            <div class="grid__cell grid__cell--m-5 grid__cell--s-12 grid__cell--12 ">
              <img loading="lazy" src="/images/establish-accountability.png" alt="Налаживаем отчетность">
            </div>
          </div>
        </div>
        <? /* <div class="home-process__button"><a class="btn btn--primary modal" href="#audit_form">Заказать аудит</a></div> */?>
      </li>
      <li class="home-process__item">
        <h3 class="home-process__title">Создаем эффективные сайты клиник
          <svg class="home-process__arrow" width="584" height="345" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
              <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
              <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
            </path>
          </svg>
        </h3>
        <div class="home-process__body">
          
            <div class="home-process__item-section">
              <div class="grid grid--align-center ">
                <div class="grid__cell grid__cell--m-3 grid__cell--s-4 grid__cell--12 ">
                  <img loading="lazy" src="/images/main-page-monitor.png" alt="Технологичные сайты" style="display: block; margin-bottom: 1.5rem; margin-left: auto; margin-right: auto;">
                </div>
                <div class="grid__cell grid__cell--m-9 grid__cell--s-8 grid__cell--12 ">
                  <h4 class="home-process__subtitle">Удобные и технологичные сайты</h4>
                  <ul class="list-marked home-process__list-marked">
                    <li>Создаем <strong>адаптивные сайты</strong>, с которыми пациенту будет удобно работать с любого устройства</li>
                    <li>Тщательно проектируем интерфейс, учитывая психологию пациентов</li>
                  </ul>
                  <h4 class="home-process__subtitle">Поисковая оптимизация</h4>
                  <ul class="list-marked home-process__list-marked">
                    <li>Разрабатываем сайты, максимально настроенные для успешного продвижения в поисковых системах</li>
                  </ul>
                  <div class="grid">
                    <div class="grid__cell grid__cell--s-2 grid__cell--3 ">
                      <picture>
                        <source srcset="/images/bitrix.webp" type="image/webp"/>
                        <source srcset="/images/bitrix.png"/><img src="/images/bitrix.png" loading="lazy" decoding="async" alt="Являемся бизнес-партнером 1c-bitrix"/>
                      </picture>
                    </div>
                    <div class="grid__cell grid__cell--s-10 grid__cell--9 "><span>Являемся бизнес-партнером лидера среди систем управления сайтом 1c-bitrix</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>

        
            <div class="home-process__item-section">
              <div class="grid grid--padding-y grid--align-center ">
                <div class="grid__cell">
                  <h4 class="home-process__subtitle">Максимальная конверсия посетителей в пациенты</h4>
                </div>
                <div class="grid__cel l grid__cell--m-6">
                  <ul class="list-marked home-process__list-marked">
                    <li>Пишем <strong>&laquo;продающие&raquo; тексты</strong> медицинских услуг, используя собственную авторскую методику</li>
                    <li>Добавляем на&nbsp;сайт материалы, увеличивающие <strong>доверие и&nbsp;информированность</strong> пациентов</li>
                    <li>Увеличиваем обращения пациентов, использую сервисы <strong>захвата пользователей</strong> <br> <strong class="home-process__superstrong">9<span>%</span></strong> конверсия сайтов в звонки пациентов после внедрения аудита</li>
                  </ul>
                </div>
                <div class="grid__cell grid__cell--m-6 ">
                  <picture>
                    <source srcset="/images/main-page-conversion.webp" type="image/webp"/>
                    <source srcset="/images/main-page-conversion.png"/><img src="/images/main-page-conversion.png" loading="lazy" decoding="async" alt="График конверсии посетителей в пациенты"/>
                  </picture>
                </div>
              </div>
            </div>
          <? /* <div class="home-process__button"><a class="btn btn--primary modal" href="#audit_form">Заказать аудит</a></div> */?>
          <? /* <div class="grid grid--justify-center  home-reasons">
            <div class="grid__cell grid__cell--m-8 grid__cell--s-10 grid__cell--12 ">
              <h4 class="home-reasons__title">4 причины заказать аудит</h4>
              <ul class="list-marked">
                <li>Вы хотите увеличить прибыль в клинике?</li>
                <li>Несмотря на все усилия, реклама не приносит пациентов?</li>
                <li>Вы хотите провести аудит работы ваших исполнителей?</li>
                <li>Вы не понимаете как систематизировать рекламу и продажи?</li>
              </ul>
            </div>
          </div>
          <div class="grid grid--padding-y" style="margin-top: 1.5rem">
            <div class="grid__cell grid__cell--m-7 grid__cell--s-12 grid__cell--12">
              <div class="youtube-responsive">
                <div class="youtube-responsive__iframe">
                  <iframe src="https://www.youtube.com/embed/9JMrSPFIQNw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
            </div>
            <div class="grid__cell grid__cell--m-5 grid__cell--s-12 grid__cell--12">
              <p><img src="/images/feedback/svetofor.svg" alt="Стоматологическая клиника Светофор" /></p>
              <p>Виталий дал ряд практических рекомендаций по продвижению клиники в интернете. На сегодняшний день мы уже внедрили часть рекомендации и я доволен полученным результатом.</p>
              <p style="font-size: .875rem; font-style: italic; line-height: 1.5; text-align: right; color: #999;">
                Казаков Виктор<br>
                руководитель клиники
              </p>
            </div>
          </div> */?>
        </div>
      </li>
      <li class="home-process__item">
        <h3 class="home-process__title">Задействуем эффективные методы рекламы
          <svg class="home-process__arrow" width="584" height="345" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
              <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
              <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
            </path>
          </svg>
        </h3>
        <div class="home-process__body">
          <div class="home-process__item-section">
            <h4 class="home-process__subtitle">Приводим пациентов из поисковых систем</h4>
            <div class="grid grid--align-center ">
              <div class="grid__cell grid__cell--m-6 ">
                <p>
                  <picture>
                    <source srcset="/images/visitors.webp" type="image/webp"/>
                    <source srcset="/images/visitors.png"/><img src="/images/visitors.png" loading="lazy" decoding="async" alt="Приводим пациентов из поисковых систем"/>
                  </picture>
                </p>
              </div>
              <div class="grid__cell grid__cell--m-6 ">
                <h5>Поисковая оптимизация</h5>
                <ul class="list-marked home-process__list-marked">
                  <li>Многократно увеличиваем посещаемость из поисковых систем</li>
                  <li>Достигаем высокой эффективности за счет использования <strong>собственной базы</strong> проверенных ключевых слов</li>
                </ul>
                <h5>Контекстная реклама</h5>
                <ul class="list-marked home-process__list-marked">
                  <li>Успешно используем <a href="/portfolio/keysi-po-kontekstnoy-reklame">низкобюджетные стратегии</a> для клиник</li>
                  <li>Снижаем стоимость клика в <strong>3-5 раз</strong></li>
                  <li>Максимизируем эффективность кампаний с помощью сервиса управления ставками</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="home-process__item-section">
            <h4 class="home-process__subtitle">Доносим положительный имидж клиники до пациентов</h4>
            <div class="grid grid--padding-y grid--align-center" style="margin-top: 0;">
              <div class="grid__cell grid__cell--m-6">
                <ul class="list-marked home-process__list-marked">
                  <li>Выявляем сайты, которые могут стать <strong>причиной оттока пациентов</strong></li>
                  <li><strong>Увеличиваем доверие</strong> к компании за счет положительного имиджа</li>
                  <li><strong>Вытесняем</strong> сайты с негативными отзывами из результатов поиска</li>
                </ul>
              </div>
              <div class="grid__cell grid__cell--m-6 ">
                <p>
                  <picture>
                    <source srcset="/images/image.webp" type="image/webp"/>
                    <source srcset="/images/image.png"/><img src="/images/image.png" loading="lazy" decoding="async" alt="Отзыв"/>
                  </picture>
                </p>
              </div>
            </div>
          </div>
          <div class="home-process__item-section">
            <h4 class="home-process__subtitle">Привлекаем пациентов из социальных сетей</h4>
            <div class="grid grid--padding-y grid--align-center" style="margin-top: 0;">
              <div class="grid__cell grid__cell--m-6 ">
                <picture>
                  <source srcset="/images/socials.webp" type="image/webp"/>
                  <source srcset="/images/socials.png"/><img src="/images/socials.png" loading="lazy" decoding="async" alt="Объявления в соцсетях"/>
                </picture>
              </div>
              <div class="grid__cell grid__cell--m-6">
                <ul class="list-marked home-process__list-marked">
                  <li>Запускаем целевые рекламные кампании в социальных сетях</li>
                  <li>Увеличиваем количество участников группы с помощью ценностного предложения</li>
                  <li>Возвращаем пациентов за счет целевого контента групп</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <? /* <div class="home-process__item-section">

          <h4 class="home-process__subtitle">Доносим положительный имидж клиники до пациентов</h4>

          <div class="grid grid--align-center ">

            <div class="grid__cell grid__cell--m-3 grid__cell--s-4 grid__cell--12 ">
                <picture>
                    <source type="image/webp" srcset="/images/home-review-resize.webp">
                    <img src="/images/home-review-resize.png" alt="">
                </picture>
            </div>

            <div class="grid__cell grid__cell--m-6 grid__cell--s-4 grid__cell--12 ">
              <ul class="list-marked home-process__list-marked">
                <li>В ходе аудита выявляем сайты, которые могут стать <strong>причиной оттока пациентов</strong></li>
                <li><strong>Увеличиваем доверие</strong> к компании за счет положительного имиджа</li>
                <li><strong>Вытесняем</strong> сайты с негативными отзывами из результатов поиска</li>
              </ul>
            </div>

          </div>

        </div> */ ?>
      </li>
    </ul>
  </div>
</section>
