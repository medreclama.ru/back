<section class="home-section">
  <h2>Обращайтесь к нам, если</h2>
  <ul class="grid  list-marked">    
    <li class="grid__cell grid__cell--m-6 ">Вас беспокоит падение прибыли клиники, вы&nbsp;волнуетесь о&nbsp;ее&nbsp;будущем
    </li>
    <li class="grid__cell grid__cell--m-6 ">Вы&nbsp;не&nbsp;понимаете как систематизировать рекламу и&nbsp;продажи в&nbsp;клинике
    </li>
    <li class="grid__cell grid__cell--m-6 ">Несмотря на&nbsp;все усилия и&nbsp;смену подрядчиков, реклама не&nbsp;приносит пациентов
    </li>
    <li class="grid__cell grid__cell--m-6 ">Не&nbsp;знаете, какая реклама даст для вас большую отдачу
    </li>
    <li class="grid__cell grid__cell--m-6 ">Несколько раз переделывали сайт, но&nbsp;результатов нет
      </li>
        
    <li class="grid__cell grid__cell--m-6 ">Вы&nbsp;вынуждены сокращать рекламные бюджеты
    </li>
  </ul>
</section>
