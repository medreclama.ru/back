<h1>Работаем на результат</h1>
<section class="home-section">
  <div class="results">
    <div class="results__list">
      <div class="results__item" target="_blank">
        <div class="results__title">300 процедур<br>в&nbsp;месяц</div>
        <div class="results__info"><strong>Клиника эстетической медицины</strong><br>Медицинский Центр Города</div>
        <a href="/portfolio/marketing/mnogoprofilnaya-klinika-meditsinskiy-tsentr-goroda" class="results__link">Подробнее</a>
      </div>
      <div class="results__item" target="_blank">
        <div class="results__title">Увеличение обращений пациентов в 2 раза</div>
        <div class="results__info"><strong>Нейрохирургический центр</strong> <br>им. профессора Г.С. Тиглиева &laquo;Новые технологии&raquo;</div>
        <a href="/portfolio/marketing/neyrohirurgicheskii-centr-novie-tekhnologii" class="results__link">Подробнее</a>
      </div>
      <div class="results__item" target="_blank">
        <div class="results__title">120 операций<br>в&nbsp;год</div>
        <div class="results__info"><strong>Клиника по пересадке волос</strong><br>Medall</div>
        <a href="/portfolio/marketing/klinika-po-peresadke-volos" class="results__link">Подробнее</a>
      </div>
      <div class="results__item" target="_blank">
        <div class="results__title">580 звонков<br>в месяц</div>
        <div class="results__info"><strong>Сеть стоматологических клиник</strong><br>Стоматологический Центр Города</div>
        <a href="/portfolio/marketing/set-stomatologicheskich-klinik-stomatologicheskiy-tsentr-goroda" class="results__link">Подробнее</a>
      </div>
      <div class="results__item" target="_blank">
        <div class="results__title">30 первичных консультаций в месяц</div>
        <div class="results__info"><strong>Центр замещения волос</strong><br>Центр Дизайна Волос</div>
        <a href="/portfolio/marketing/salon-krasoti-tsentr-dizayna-volos" class="results__link">Подробнее</a>
      </div>
    </div>
  </div>
</section>
