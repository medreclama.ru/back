<section class="home-section">
  <h2>О нас говорят</h2>
  <div class="home-feedback">
    <div class="grid grid--justify-center ">
      <div class="grid__cell grid__cell--m-10 grid__cell--s-12 home-feedback__slides">
        <ul class="slides">
          <li>
            <div class="grid  feedback-item">
              <div class="grid__cell grid__cell--m-2 grid__cell--s-2 grid__cell--4 feedback-item__photo"><img class="image-border" loading="lazy" src="/images/feedback-treiger.jpg" alt=""/>
              <a class="feedback-item__link btn btn--secondary" href="/images/feedback/1-clinic-big.jpg" data-fancybox="data-fancybox">Скан отзыва</a>
              </div>
              <div class="grid__cell grid__cell--m-10 grid__cell--s-12 grid__cell--12 feedback-item__info">
                <div class="feedback-item__body">Несмотря на небольшой объем поставленных перед агентством задач, его сотрудники успели зарекомендовать себя как грамотное и эффективное агентство, вникающие в тонкости бизнеса клиента, как в свои собственные.</div>
                <div class="feedback-item__author"><strong>Многопрофильная клиника «Первая семейная клиника Петербурга»</strong><span>Трейгер А.З.</span><span>Генеральный директор</span></div>
              </div>
            </div>
          </li>
          <li>
            <div class="grid  feedback-item">
                <div class="grid__cell grid__cell--m-2 grid__cell--s-2 grid__cell--4 feedback-item__photo">
                  <img class="image-border" loading="lazy" src="/images/feedback-skvortsova.jpg" alt="" draggable="false">
                  <a class="feedback-item__link btn btn--secondary" href="/images/feedback/stomadeus-big.jpg" data-fancybox="data-fancybox">Скан отзыва</a>
                </div>
                <div class="grid__cell grid__cell--m-10 grid__cell--s-12 grid__cell--12 feedback-item__info">
                  <div class="feedback-item__body">Посещаемость сайта в результате работы агентства постоянно росла и за год увеличилась более чем в 3 раза. Значительно выросло и количество звонков. Надеемся на дальнейшее плодотворное сотрудничество</div>
                  <div class="feedback-item__author"><strong>Сеть стоматологических клиник «Стомадеус»</strong><span>Скворцова Т.В.</span><span>Управляющая</span></div>
                </div>
              </div>
          </li>
          <li>
            <div class="grid  feedback-item">
              <div class="grid__cell grid__cell--m-2 grid__cell--s-2 grid__cell--4 feedback-item__photo">
                <img class="image-border" loading="lazy" src="/images/feedback-moiseychikov.jpg" alt="" draggable="false">
                <a class="feedback-item__link btn btn--secondary" href="/images/feedback/whitedent-big.jpg" data-fancybox="data-fancybox">Скан отзыва</a>
              </div>
              <div class="grid__cell grid__cell--m-10 grid__cell--s-12 grid__cell--12 feedback-item__info">
                <div class="feedback-item__body">Работать с ребятами просто приятно. Работают активно, профессионально, решают поставленные задачи без промедления. Результаты работы стали видны практически через месяц нашего сотрудничества. Повысилась узнаваемость компании. После грамотной оптимизации сайта возросло количество посещений сайта в 2,5 раза. Увеличилось количество звонков в клинику в 2 раза, и, практически, 80% процентов позвонивших, стали нашими клиентами.</div>
                <div class="feedback-item__author"><strong>Стоматологическая клиника «Вайт-Дент»</strong><span>Моисейчиков Андрей Евгеньевич</span><span>Генеральный директор</span></div>
              </div>
            </div>
          </li>
          <li>
            <div class="grid  feedback-item">
              <div class="grid__cell grid__cell--m-2 grid__cell--s-2 grid__cell--4 feedback-item__photo">
                <img class="image-border" loading="lazy" src="/images/feedback-boyarskaya.jpg" alt=""/>
                <a class="feedback-item__link btn btn--secondary" href="/images/feedback/netvolos-big.webp" data-fancybox="data-fancybox">Скан отзыва</a>
              </div>
              <div class="grid__cell grid__cell--m-10 grid__cell--s-12 grid__cell--12 feedback-item__info">
                <div class="feedback-item__body">В течении всего времени Виталий Заря и команда агентства «Медреклама» поставляли нам много пациентов из интернета. Хочу выразить благодарность Виталию Заря за консультативную работу и работу по пролдвижению сайта в интернете.</div>
                <div class="feedback-item__author"><strong>Клиника по пересадке волос MEDALL</strong><span>Боярская Светлана Валерьевна</span><span>Заведующая отделением трансплантации волос</span></div>
              </div>
            </div>
          </li>
          <li>
            <div class="grid  feedback-item">
              <div class="grid__cell grid__cell--m-2 grid__cell--s-2 grid__cell--4 feedback-item__photo">
                <img class="image-border" loading="lazy" src="/images/feedback-roslyakova.jpg" alt=""/>
                <a class="feedback-item__link btn btn--secondary" href="/images/feedback/aurmed-big.jpg" data-fancybox="data-fancybox">Скан отзыва</a>
              </div>
              <div class="grid__cell grid__cell--m-10 grid__cell--s-12 grid__cell--12 feedback-item__info">
                <div class="feedback-item__body">В результате проведенных работ, значительно укрепился статус нашей компании, и повысилось узнаваемость наших услуг.<br>Хотелось бы выразить свою благодарность Заря Виталию за ответственность, оперативность, внимательность, а также своевременное реагирование на наши поручения.</div>
                <div class="feedback-item__author"><strong>Аюрведический медицинский центр Shankara</strong><span>Ираида Рослякова</span><span>Руководитель</span></div>
              </div>
            </div>
          </li>
          <li>
            <div class="grid  feedback-item">
              <div class="grid__cell grid__cell--m-2 grid__cell--s-2 grid__cell--4 feedback-item__photo">
                <img class="image-border" loading="lazy" src="/images/feedback-leites.jpg" alt=""/>
              </div>
              <div class="grid__cell grid__cell--m-10 grid__cell--s-12 grid__cell--12 feedback-item__info">
                <div class="feedback-item__body">В настоящее время к нам постоянно поступают предложения по продвижению сайта от других компаний. Но я четко знаю, что такого персонализированного подхода я не получу больше нигде. По сути, мне помогают развивать бизнес. А не просто дублируют его развитие в Интернет-пространстве.</div>
                <div class="feedback-item__author"><strong>Салон красоты «Центр Дизайна Волос»</strong><span>Ольга Лейтес</span><span>Генеральный директор</span></div>
              </div>
            </div>
          </li>
          <li>
            <div class="grid  feedback-item">
              <div class="grid__cell grid__cell--m-2 grid__cell--s-2 grid__cell--4 feedback-item__photo">
                <img class="image-border" loading="lazy" src="/images/feedback-metelev.jpg" alt=""/>
                <a class="feedback-item__link btn btn--secondary" href="/images/feedback/mcg-big.jpg" data-fancybox="data-fancybox">Скан отзыва</a>
              </div>
              <div class="grid__cell grid__cell--m-10 grid__cell--s-12 grid__cell--12 feedback-item__info">
                <div class="feedback-item__body">От лица клиники «Медицинский центр города» хотим выразить искреннюю благодарность Заря Виталию за плодотворное сотрудничество.<br>Хочется отметить оперативность и чуткость работников агентства в решении вопросов и чёткость выдаваемых рекомендаций.</div>
                <div class="feedback-item__author"><strong>Многопрофильная клиника «Медицинский Центр Города»</strong><span>Дмитрий Метелев</span><span>Генеральный директор</span></div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="home-feedback__link"><a href="/reports">все рекомендации</a></div>
  </div>

</section>
