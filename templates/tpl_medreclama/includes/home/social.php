<section class="home-section" id="social">
  <h2>Узнайте секреты привлечения пациентов</h2>
  <h3 class="home-subtitle">Подписывайтесь на наши группы</h3>
  <div class="grid">
    <div class="grid__cell grid__cell--m-4 grid__cell--s-6 grid__cell--12 ">
      <!-- <script type="text/javascript" src="//vk.com/js/api/openapi.js?151"></script> -->
      <div id="vk_groups"></div>
      <!-- <script type="text/javascript">VK.Widgets.Group("vk_groups", {mode: 3, width: 304, height: 500}, 86134177);</script> -->
    </div>
    <div class="grid__cell grid__cell--m-4 grid__cell--s-6 grid__cell--12 ">
      <div class="youtube-module">
        <div class="youtube-module__title">Мой канал на Youtube по маркетингу для клиник</div>
        <ul class="list-marked youtube-module__list">
          <li>Обучающие видео</li>
          <li>Записи вебинаров</li>
          <li>Интервью</li>
        </ul>
        <!-- <script src="https://apis.google.com/js/platform.js"></script> -->
        <div class="g-ytsubscribe" data-channelid="UC8OPJfPmALH-GRmDIe1DPLQ" data-layout="full" data-count="default"></div>
      </div>
    </div>
  </div>
</section>
