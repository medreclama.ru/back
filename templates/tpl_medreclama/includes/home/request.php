<section class="home-section" id="request">
  <h2>Получить коммерческое предложение</h2>
  <div class="home-form">

        <form
          class="home-form__form home-form__request validated-form submitJS grid grid--padding-y grid--justify-space-between"
          method="post"
          action="/index.php?option=com_rsform&view=rsform&formId=21"
          
        >
          
        <input type="text" name="req_name" value="">
        <div class="grid__cell grid__cell--m-7 grid__cell--12 ">
          <div class="home-form__input">
            <label class="home-form__label" for="home-form__task">Краткое описание задачи</label>
            <textarea class="form-input" id="home-form__task"> </textarea>
          </div>
          <div class="grid">


          <div class="home-form__input grid__cell grid__cell--m-6 grid__cell--12">
            <label class="home-form__label home-form__label--required" for="home-form__name">Имя</label>
            <input class="form-input validated required regex" data-regex="[а-яё]" id="home-form__name" name="form[fio]">
            <div class="validation-msg validation-msg-required"></div>
            <div class="validation-msg validation-msg-regex">Должны быть русские буквы</div>
          </div>

          <div class="home-form__input grid__cell grid__cell--m-6 grid__cell--12">
            <label class="home-form__label home-form__label--required" for="home-form__email">Электронная почта</label>
            <input class="form-input validated required" id="home-form__email" name="form[email]">
            <div class="validation-msg validation-msg-required"></div>
          </div>
        
          <div class="home-form__input grid__cell grid__cell--m-6 grid__cell--12">
            <label class="home-form__label home-form__label--required" for="home-form__tel">Телефон</label>
            <input class="form-input validated required" id="home-form__tel" type="tel" name="form[phone]">
            <div class="validation-msg validation-msg-required"></div>
          </div>

          <div class="home-form__input grid__cell grid__cell--m-6 grid__cell--12">
            <label class="home-form__label home-form__label--required" for="home-form__site">Ваш сайт</label>
            <input class="form-input validated required" id="home-form__site" name="form[site]">
            <div class="validation-msg validation-msg-required"></div>
          </div>
          </div>

          <? /* <div class="radio_block">
            <div class="radio_block-inner validated-type-radio">

              <div class="home-form__input home-form__input--radio">
                <input
                  type="radio"
                  name="form[service]"
                  id="home-form__service-1"
                  class="validated required"
                  value="Комплексный аудит интернет-маркетинга"
                >
                <label class="home-form__label" for="home-form__service-1">Комплексный аудит интернет-маркетинга</label>
              </div>

              <div class="home-form__input home-form__input--radio">
                <input
                  type="radio"
                  name="form[service]"
                  id="home-form__service-2"
                  class="validated required"
                  value="Аудит продающих качеств сайта"
                >
                <label class="home-form__label" for="home-form__service-2">Аудит продающих качеств сайта</label>
              </div>

              <div class="home-form__input home-form__input--radio">
                <input
                  type="radio"
                  name="form[service]"
                  id="home-form__service-4"
                  class="validated required"
                  value="Аудит воронки продаж клиники"
                >
                <label class="home-form__label" for="home-form__service-4">Аудит воронки продаж клиники</label>
              </div>

              <div class="home-form__input home-form__input--radio">
                <input
                  type="radio"
                  name="form[service]"
                  id="home-form__service-3"
                  class="validated required"
                  value="Консультация"
                >
                <label class="home-form__label" for="home-form__service-3">Консультация</label>
              </div>

            </div>
            <div class="validation-msg validation-msg-required">Обязательное поле</div>
          </div>
          */ ?>
          <script>document.write('<input type="hidden" name="form[formId]" value="21"/>')</script>
          </div>
          
          <div class="grid__cell grid__cell--m-4 grid__cell--12 home-form__info"><span><strong>Заря Виталий</strong> 
                        <p class="text-marked text-marked--size--xs">Руководитель агентства
                        </p></span><a href="mailto:sale@medreclama.ru">sale@medreclama.ru </a>
                      <div class="text-marked text-marked--size--xs">Ответим в течение одного рабочего дня
                      </div>
                      <div class="home-form__submit">
                        <button class="btn btn--primary" type="submit">Отправить заявку</button>
                      </div>
                      <p class="text-marked text-marked--size--xs">Оставляя заявку, вы соглашаетесь на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh">обработку персональных данных</a>
                      </p>
                    </div>
          
        </form>

  </div>
</section>
