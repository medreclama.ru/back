<section class="home-section">            
  <div class="page-section">                     
    <h2>Наши кейсы                               </h2>
  </div>
  <div class="home-cases slider"> 
    <ul class="slides">
      <li>
        <a class="portfolio-promotion-item portfolio-promotion-item--neuros" href="/portfolio/marketing/neyrohirurgicheskii-centr-novie-tekhnologii">
          <p class="portfolio-promotion-item__title">Увеличение количества обращений пациентов за год в 2 раза</p>
          <p class="portfolio-promotion-item__name">Нейрохирургический центр им. профессора Г.С. Тиглиева «Новые технологии»</p>
          <p class="portfolio-promotion-item__category" data-category="Медицина и здоровье">Медицина и здоровье</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-neuros.svg" alt="Нейрохирургический центр им. профессора Г.С. Тиглиева «Новые технологии»" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-neuros.svg" alt="Увеличение количества обращений пациентов за год в 2 раза" loading="lazy" decoding="async"/></a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--stomatology-center portfolio-list__col-m-6" href="/portfolio/marketing/set-stomatologicheskich-klinik-stomatologicheskiy-tsentr-goroda">
                  <p class="portfolio-promotion-item__title">600 обращений пациентов из&nbsp;интернета в&nbsp;месяц</p>
                  <p class="portfolio-promotion-item__name">Сеть клиник «Стоматологический центр города»</p>
                  <p class="portfolio-promotion-item__category" data-category="Медицина и здоровье">Медицина и здоровье</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-stomatology-center.svg" alt="Сеть клиник «Стоматологический центр города»" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-stomatology-center.svg" alt="Достижение 600 обращений пациентов из&nbsp;интернета в&nbsp;месяц" loading="lazy" decoding="async"/>
                </a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--medall-hair portfolio-list__col-m-6" href="/portfolio/marketing/klinika-po-peresadke-volos">
                <p class="portfolio-promotion-item__title">Увеличение количества пациентов, привлеченных из&nbsp;интернета, на&nbsp;58%</p>
                <p class="portfolio-promotion-item__name">Клиника пересадки волос «MEDALL»</p>
                <p class="portfolio-promotion-item__category" data-category="Медицина и здоровье">Медицина и здоровье</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-medall-hair.svg" alt="Клиника пересадки волос «MEDALL»" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-medall-hair.svg" alt="Увеличение количества пациентов, привлеченных из&nbsp;интернета, на&nbsp;58%" loading="lazy" decoding="async"/></a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--33zub portfolio-list__col-m-6" href="/portfolio/marketing/stomatologicheskaya-klininka-33zub">
                  <p class="portfolio-promotion-item__title">Привлечение 43 конверсий для сети стоматологических клиник в низкий сезон</p>
                  <p class="portfolio-promotion-item__name">Стоматологические клиники 33zub</p>
                  <p class="portfolio-promotion-item__category" data-category="Медицина и здоровье">Медицина и здоровье</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-33zub.png" alt="Стоматологические клиники 33zub" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-stomadeus.svg" alt="Привлечение 43 конверсий для сети стоматологических клиник в низкий сезон" loading="lazy" decoding="async"/>
                </a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--medall portfolio-list__col-m-6" href="/portfolio/marketing/mnogoprofilnaya-klinika-meditsinskiy-tsentr-goroda">
                <p class="portfolio-promotion-item__title">Увеличение количества обращений пациентов из Интернет в 2,2 раза</p>
                <p class="portfolio-promotion-item__name">Многопрофильная клиника «Медицинский Центр Города»</p>
                <p class="portfolio-promotion-item__category" data-category="Медицина и здоровье">Медицина и здоровье</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-medcenter.webp" alt="Клиника «Медицинский Центр Города»" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-medall.svg" alt="Увеличение количества обращений пациентов из Интернет в 2,2 раза" loading="lazy" decoding="async"/></a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--whitedent portfolio-list__col-m-6" href="/portfolio/marketing/klinika-esteticheskoy-stomatologii-vaytdent">
                  <p class="portfolio-promotion-item__title">Увеличение количества первичных записей пациентов из Интернет до 140 в месяц</p>
                  <p class="portfolio-promotion-item__name">Стоматологическая клиника «ВайтДент»</p>
                  <p class="portfolio-promotion-item__category" data-category="Медицина и здоровье">Медицина и здоровье</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-whitedent.webp" alt="Стоматологическая клиника «ВайтДент»" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-whitedent.svg" alt="Увеличение количества первичных записей пациентов из Интернет до 140 в месяц" loading="lazy" decoding="async"/>
                </a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--ignatovskogo portfolio-list__col-m-6" href="/portfolio/marketing/klinika-ignatovskogo">
                  <p class="portfolio-promotion-item__title">Увеличении количество звонков первичных пациентов на&nbsp;24% по&nbsp;каналу SEO</p>
                  <p class="portfolio-promotion-item__name">Клиника Дерматологии и Репродуктивного здоровья доктора Игнатовского</p>
                  <p class="portfolio-promotion-item__category" data-category="Медицина и здоровье">Медицина и здоровье</p><img class="portfolio-promotion-item__logo portfolio-promotion-item__logo--left" src="/images/portfolio/prodvizhenie-saytov/logo-ignatovskogo.webp" alt="Клиника Дерматологии и Репродуктивного здоровья доктора Игнатовского" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-ignatovskogo.svg" alt="Увеличении количество звонков первичных пациентов на&nbsp;24% по&nbsp;каналу SEO" loading="lazy" decoding="async"/>
                </a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--stomadeus portfolio-list__col-m-6" href="/portfolio/marketing/set-stomatologicheskiy-klinik-stomadeus">
                <p class="portfolio-promotion-item__title">Увеличение количества звонков пациентов из&nbsp;интернета в&nbsp;2,5 раза</p>
                <p class="portfolio-promotion-item__name">Сеть стоматологических клиник «Стомадеус»</p>
                <p class="portfolio-promotion-item__category" data-category="Медицина и здоровье">Медицина и здоровье</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-stomadeus.svg" alt="Сеть стоматологических клиник «Стомадеус»" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-stomadeus.svg" alt="Увеличение количества звонков пациентов из&nbsp;интернета в&nbsp;2,5 раза" loading="lazy" decoding="async"/></a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--medall-contest portfolio-list__col-m-6" href="/portfolio/marketing/klininka-medall">
                  <p class="portfolio-promotion-item__title">Разработка механики и проведение конкурса для клиники эстетической медицины</p>
                  <p class="portfolio-promotion-item__name">Клиника  эстетической медицины «MEDALL»</p>
                  <p class="portfolio-promotion-item__category" data-category="Медицина и здоровье">Медицина и здоровье</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-medall-2.png" alt="Клиника «MEDALL»" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-medall-hair.svg" alt="Разработка механики и проведение конкурса для клиники эстетической медицины" loading="lazy" decoding="async"/>
                </a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--dhcentre portfolio-list__col-m-6" href="/portfolio/marketing/stomatologicheskiy-centr-dhc">
                  <p class="portfolio-promotion-item__title">Реализация эффективной рекламной стратегии в короткие сроки</p>
                  <p class="portfolio-promotion-item__name">Стоматологический центр DHC</p>
                  <p class="portfolio-promotion-item__category" data-category="Медицина и здоровье">Медицина и здоровье</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-dhc.webp" alt="Стоматологический центр DHC" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-medall-hair.svg" alt="Реализация эффективной рекламной стратегии в короткие сроки" loading="lazy" decoding="async"/>
                </a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--hairdesign portfolio-list__col-m-6" href="/portfolio/marketing/salon-krasoti-tsentr-dizayna-volos">
                  <p class="portfolio-promotion-item__title">Увеличение количества обращений клиентов в&nbsp;10&nbsp;раз в&nbsp;течении 15&nbsp;лет</p>
                  <p class="portfolio-promotion-item__name">Центр замещения&nbsp;&mdash; &laquo;Центр Дизайна Волос&raquo;</p>
                  <p class="portfolio-promotion-item__category" data-category="Индустрия красоты">Индустрия красоты</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-hairdesign.svg" alt="Салон красоты «Центр Дизайна Волос»" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-hairdesign.svg" alt="Увеличение количества обращений клиентов в 10 раз в течении 15 лет" loading="lazy" decoding="async"/>
                </a>
      </li>
      <li>
      <a class="portfolio-promotion-item portfolio-promotion-item--vsepariki portfolio-list__col-m-6" href="/portfolio/marketing/magazin-parikov-vsepariki">
                <p class="portfolio-promotion-item__title">Увеличение продаж изделий из волос в 6 раз</p>
                <p class="portfolio-promotion-item__name">Интернет-магазин париков</p>
                <p class="portfolio-promotion-item__category" data-category="Индустрия красоты">Индустрия красоты</p><img class="portfolio-promotion-item__logo" src="/images/portfolio/prodvizhenie-saytov/logo-vsepariki.svg" alt="Интернет-магазин париков" loading="lazy" decoding="async"/><img class="portfolio-promotion-item__chart" src="/images/portfolio/prodvizhenie-saytov/chart-hairdesign.svg" alt="Увеличение продаж изделий из волос в 6 раз" loading="lazy" decoding="async"/></a>
      </li>
    </ul>
  </div>
  <? /* <div class="text-align-center"><a class="btn btn--primary" href="/">Оценить проект</a></div> */?>
</section>
