<section class="home-section">
    <h2>Направления, с которыми мы успешно работали</h2>
    <ul class="home-directions">
    <li class="home-directions__item">Стоматология</li>
    <li class="home-directions__item">Пластическая хирургия</li>
    <li class="home-directions__item">Косметология и дерматология</li>
    <li class="home-directions__item">Урология и гинекология</li>
    <li class="home-directions__item">Пересадка волос</li>
    <li class="home-directions__item">Нейрохирургия</li>
    <li class="home-directions__item">Флебология</li>
    <li class="home-directions__item">Аюрведическая медицина</li>
    <li class="home-directions__item">Остеопатия</li>
    <li class="home-directions__item">Снижение веса</li>
    <li class="home-directions__item">Трихология</li>
    <li class="home-directions__item">Диагностика</li>
    </ul>
</section>
