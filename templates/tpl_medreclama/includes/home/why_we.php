<section class="home-section">
  <h2>Почему мы?</h2>
  <div class="why-we">
    <ul class="grid  why-we__list">
      <li class="grid__cell grid__cell--m-3 grid__cell--s-6 why-we__item">
        <div class="why-we__title">Специализация</div>
        <div class="why-we__info">Наш опыт и&nbsp;знания сконцентрированы на <strong>медицинских проектах</strong></div>
      </li>
      <li class="grid__cell grid__cell--m-3 grid__cell--s-6 why-we__item">
        <div class="why-we__title">Ответственность</div>
        <div class="why-we__info">За&nbsp;результаты по&nbsp;вашему проекту лично отвечает <strong>руководитель агентства</strong></div>
      </li>
      <li class="grid__cell grid__cell--m-3 grid__cell--s-6 why-we__item">
        <div class="why-we__title">Стабильность</div>
        <div class="why-we__info">95% клиентов работают с&nbsp;нами уже <strong>много лет</strong></div>
      </li>
      <li class="grid__cell grid__cell--m-3 grid__cell--s-6 why-we__item">
        <div class="why-we__title">15 лет</div>
        <div class="why-we__info">Опыт работы по&nbsp;продвижению клиник с&nbsp;<strong>2008 года</strong></div>
      </li>
    </ul>
  </div>
</section>

<? /* <section class="home-section">
  <h2>Почему мы?</h2>
  <div class="why-we">
    <ul class="grid  why-we__list">
      <li class="grid__cell grid__cell--m-4 grid__cell--s-6 why-we__item">
        <div class="why-we__title">Стабильность результата</div>
        <div class="why-we__info">95% клиентов работают с нами уже много лет</div>
      </li>
      <li class="grid__cell grid__cell--m-4 grid__cell--s-6 why-we__item">
        <div class="why-we__title">Компетенции в сфере медицинского маркетинга</div>
        <div class="why-we__info">Опыт работы по продвижению клиник с 2002 года</div>
      </li>
      <li class="grid__cell grid__cell--m-4 grid__cell--s-6 why-we__item">
        <div class="why-we__title">Ответственность</div>
        <div class="why-we__info">За результаты по вашему проекту лично отвечает руководитель агентства</div>
      </li>
      <li class="grid__cell grid__cell--m-4 grid__cell--s-6 why-we__item">
        <div class="why-we__title">Проверенные методы</div>
        <div class="why-we__info">Опробованные рекламные стратегии для различного типа клиник</div>
      </li>
      <li class="grid__cell grid__cell--m-4 grid__cell--s-6 why-we__item">
        <div class="why-we__title">Сертифицированные специалисты</div>
        <div class="why-we__info">Сотрудники имеют сертификаты по всем направлениям интернет рекламы</div>
      </li>
    </ul>
  </div>
</section> */?>
