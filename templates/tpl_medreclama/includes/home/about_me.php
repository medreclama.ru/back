<? /* <section class="home-section">
  <div class="about-me">

    <div class="about-me__photo">
        <picture>
            <source type="image/webp" srcset="/images/about-me-photo.webp">
            <img src="/images/about-me-photo.jpg" alt="Виталий Заря">
        </picture>

    </div>

    <div class="about-me__info">
      <p>Здравствуйте. Меня зовут Виталий Заря&nbsp;&mdash; я&nbsp;эксперт по&nbsp;интернет-маркетингу в&nbsp;сфере медицины и&nbsp;индустрии красоты, консультант, <strong>практикующий маркетолог</strong>, руководитель агентства медицинского маркетинга &laquo;Медреклама&raquo;.</p>
      <p>Более <strong>15&nbsp;лет</strong> в&nbsp;сфере интернет-маркетинга. Выполнил более <strong>50&nbsp;аудитов интернет-маркетинга клиник</strong> и&nbsp;салонов красоты. Автор низкобюджетных стратегий для продвижения клиник, книги &laquo;Конкурентные преимущества в&nbsp;рекламе клиник&raquo;.</p>
      <p>В&nbsp;настоящее время <strong>провожу аудиты маркетинга клиник</strong>, руковожу группой специалистов, которые на&nbsp;практике реализуют задачи по&nbsp;<strong>увеличению продаж в&nbsp;клиниках</strong>.</p>
      <blockquote>Фокусировка на&nbsp;медицинском бизнесе дала возможность собрать компетенции, базу знаний, опробовать на&nbsp;практике ряд подходов и&nbsp;стратегий.</blockquote>
    </div>

  </div>
</section> */ ?>
