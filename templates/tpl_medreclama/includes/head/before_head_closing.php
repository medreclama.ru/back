<script type="text/javascript">
$(document).ready(function(){
    <?
    if(strpos($_SERVER['REQUEST_URI'], 'utm_medium=cpc')): ?>
        $.cookie('user_came_from_channel', 'context', {
            expires: 14,
            path: '/'
        });<?
    endif;

    if(strpos($_SERVER['REQUEST_URI'], 'utm_term')): ?>
      var utm_term = $.getUrlVar('utm_term');	// ищем параметр в url

      if(utm_term !== undefined){  //если параметр передан
        utm_term = decodeURI(utm_term);
        if(utm_term.indexOf('#') > 0) {
          utm_term = utm_term.substring(0, utm_term.indexOf('#'));
        }
        $.cookie('user_from_context_term', utm_term, {
          expires: 14,
          path: '/'
        });
      }<?
    endif;


    if(isset($_SERVER['HTTP_REFERER']) AND $_SERVER['HTTP_REFERER']):
      $referer = $_SERVER['HTTP_REFERER'];

      $search_engine = '';
      if(strpos($referer, 'yandex')) 	$search_engine = 'Yandex.ru';
      if(strpos($referer, 'google')) 	$search_engine = 'Google.ru';
      if(strpos($referer, 'mail')) 	$search_engine = 'Mail.ru';
      if(strpos($referer, 'rambler')) $search_engine = 'Rambler.ru';
      if(strpos($referer, 'bing')) 	$search_engine = 'Bing.com';
      if(strpos($referer, 'yahoo')) 	$search_engine = 'Yahoo.com';
      if(strpos($referer, 'ask.fm')) 	$search_engine = 'Ask.fm';
      if(strpos($referer, 'webalta')) $search_engine = 'Webalta.ru';
      if(strpos($referer, 'nigma')) 	$search_engine = 'Nigma.ru';

      if($search_engine): ?>
        $.cookie(
          'user_came_from_channel',
          '<?= $search_engine ?>',
          {
            expires: 14,
            path: '/'
          }
        );
        $.removeCookie('user_from_context_term', { path: '/' });
      <? endif ?>

    <? endif ?>
});
</script>

<?/* <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35671688-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script> */?>
