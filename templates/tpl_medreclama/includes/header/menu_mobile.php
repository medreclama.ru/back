<div class="header-mobile__logo">
  <div class="header-mobile-logo"><a href="/"></a></div>
</div>

<div class="header-mobile__info">
  <jdoc:include type="modules" name="header_info-mobile" style="raw" />
</div>

<div class="header-mobile__menu">

  <input class="header-mobile-switcher" type="checkbox" id="header-mobile-switcher">

  <label class="header-mobile-switcher-label" for="header-mobile-switcher">
    <span></span>
    <span></span>
    <span></span>
  </label>

  <div class="header-mobile-menu">
    <jdoc:include type="modules" name="header_menu-mobile" style="raw" />
  </div>

</div>
