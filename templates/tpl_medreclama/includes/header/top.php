<div class="content">
  <div class="header-contents">

    <div class="header-contents__item header-contents__item--logo">
      <div class="header-logo"><a href="/"></a></div>
    </div>

    <div class="header-contents__item header-contents__item--info">
      <div class="header-info">
        <div class="header-info__inner">
          <jdoc:include type="modules" name="header_info" style="raw" />
        </div>
      </div>
    </div>

    <div class="header-contents__item header-contents__item--links">
      <jdoc:include type="modules" name="header_links" style="raw" />
    </div>

    <div class="header-contents__item header-contents__item--contacts">
      <div class="header-contacts">
        <jdoc:include type="modules" name="header_contacts" style="raw" />
      </div>
    </div>

  </div>
</div>