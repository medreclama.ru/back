<?/* <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','//connect.facebook.net/en_US/fbevents.js');

  fbq('init', '1525072397785517');
  fbq('track', "PageView");
</script>
<noscript>
  <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1525072397785517&ev=PageView&noscript=1" alt="" />
</noscript>
<!-- End Facebook Pixel Code --> */?>

<!-- Facebook Pixel Code BOOKS -->
<? /* 
if(
  strpos($_SERVER['REQUEST_URI'], 'kniga') ||
  strpos($_SERVER['REQUEST_URI'], 'konkurentnie-preimuschestva-v-reklame-kliniki')
):
?>
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');

      fbq('init', '1525072397785517');
      fbq('track', "PageView");
      fbq('track', 'Lead');
    </script>
    <noscript>
      <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1525072397785517&ev=PageView&noscript=1" alt="" />
    </noscript>
    <!-- End Facebook Pixel Code -->
<? endif ?>
<!-- End Facebook Pixel Code BOOKS --> */?>

<? /* <script type="text/javascript">
  !function(){
    var t=document.createElement("script");
    t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?157",t.onload=function(){
      VK.Retargeting.Init("VK-RTRG-262191-i0HBE"),VK.Retargeting.Hit()
    },document.head.appendChild(t)
  }();
</script>
<noscript>
  <img src="https://vk.com/rtrg?p=VK-RTRG-262191-i0HBE" style="position:fixed; left:-999px;" alt=""/>
</noscript> */?>

<header class="header-main">

  <div class="container header-main__top hidden-s hidden-xs">
    <? include __DIR__."/header/top.php" ?>
  </div>

  <div class="container header-main__bottom hidden-s hidden-xs">
    <div class="content">
      <jdoc:include type="modules" name="header_menu" style="raw" />
    </div>
  </div>

  <div class="header-main__mobile hidden-hd hidden-xl hidden-l hidden-m">
    <div class="header-mobile">
      <? include __DIR__."/header/menu_mobile.php" ?>
    </div>
  </div>

</header>
