<!DOCTYPE html>
<html lang="ru">

  <head>
    <? include __DIR__."/includes/head.php" ?>
  </head>

  <body>
    <? include __DIR__."/includes/body/after_body_open.php" ?>

    <div class="page">
      <? include __DIR__."/includes/header.php" ?>
      <? include __DIR__."/includes/main.php" ?>
      <? include __DIR__."/includes/footer.php" ?>
	  <? include __DIR__."/includes/body/book_module/index.php" ?>
    </div>

    <? include __DIR__."/includes/body/before_body_close.php" ?>
    <? include __DIR__."/includes/footer/scripts.php" ?>
  </body>

</html>