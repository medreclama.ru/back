<? defined('_JEXEC') or die ?>
<div class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">

	<?
	// Get rid of duplicated entries on trail including home page when using multilanguage
	for ($i = 0; $i < $count; $i++)
	{
		if ($i === 1 && !empty($list[$i]->link) && !empty($list[$i - 1]->link) && $list[$i]->link === $list[$i - 1]->link)
		{
			unset($list[$i]);
		}
	}

	// Find last and penultimate items in breadcrumbs list
	end($list);
	$last_item_key   = key($list);
	prev($list);
	$penult_item_key = key($list);

	// Make a link if not the last item in the breadcrumbs
	$show_last = $params->get('showLast', 1);

	// Generate the trail
	foreach ($list as $key => $item) :
		if ($key !== $last_item_key) :
			// Render all but last item - along with separator ?>
			<div class="breadcrumbs__parent" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
				<? if (!empty($item->link)) : ?>
					<a itemprop="item" href="<?= $item->link ?>"><span itemprop="name"><?= $item->name ?></span></a>
				<? else : ?>
					<span itemprop="name">
						<?= $item->name ?>
					</span>
				<? endif ?>
				<meta itemprop="position" content="<?= $key + 1 ?>">
			</div>

      <? if (($key !== $penult_item_key) || $show_last) : ?>
        <div class="breadcrumbs__separator"></div>
      <? endif ?>

		<? elseif ($show_last) :
			// Render last item if reqd. ?>
			<div class="breadcrumbs__current" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
				<span itemprop="name">
					<?= $item->name ?>
				</span>
				<meta itemprop="position" content="<?= $key + 1 ?>">
			</div>
		<? endif;
	endforeach ?>
</div>
