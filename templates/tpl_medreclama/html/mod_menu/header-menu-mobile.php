<?
defined('_JEXEC') or die;

// The menu class is deprecated. Use nav instead
?>
<ul class="header-mobile-menu__list">
<?php foreach ($list as $i => &$item)
{
	$class = 'header-mobile-menu__item';

	if (in_array($item->id, $path))
	{
		$class .= ' header-nav__item--active';
	}
	elseif ($item->type === 'alias')
	{
		$aliasToId = $item->params->get('aliasoptions');

		if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
		{
			$class .= ' active';
		}
		elseif (in_array($aliasToId, $path))
		{
			$class .= ' alias-parent-active';
		}
	}

	if ($item->type === 'separator')
	{
		$class .= ' divider';
	}

	if ($item->parent)
	{
		$class .= ' mobile-nav__item--parent';
	}

	echo '<li class="' . $class . '">';

	if($item->level == 1): ?>
    <div class="header-mobile-menu__item-body">
	<? endif;

	switch ($item->type) :
		case 'separator':
		case 'component':
		case 'heading':
		case 'url':
			require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
			break;

		default:
			require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
			break;
	endswitch;

	if($item->level == 1): ?>
    </div>
	<? endif;

	// The next item is deeper.
	if ($item->deeper)
	{
		echo '<ul class="header-mobile-menu__sublist">';
	}
	// The next item is shallower.
	elseif ($item->shallower)
	{
		echo '</li>';
		echo str_repeat('</ul></li>', $item->level_diff);
	}
	// The next item is on the same level.
	else
	{
		echo '</li>';
	}
}
?></ul>
