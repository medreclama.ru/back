<? defined('_JEXEC') or die ?>
<h2 class="margin-top"><?= $module->title ?></h2>

<div class="blog-interesting">
    <div class="grid grid--padding-y  blog-interesting__list">
    <? foreach ($list as $item) : ?>

        <? $images = json_decode($item->images) ?>

        <div class="grid__cell grid__cell--m-3 grid__cell--6 blog-interesting__item">

            <? if(!empty($images->image_intro)): ?>
            <a class="blog-interesting__image" href="<?= $item->link ?>">
                <img
                    src="/<?= $images->image_intro ?>"
                    alt="<?= $item->title ?>"
                    title="<?= $item->title ?>"
                />
            </a>
            <? endif ?>

            <a
                class="blog-interesting__name"
                href="<?= $item->link ?>"
            ><?= $item->title ?></a>

            <div class="blog-interesting__views"><?= $item->hits ?></div>
            
        </div>

    <? endforeach ?>
    </div>
</div>
