<?php
/**
 * @package		Joomla.Site
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

function modChrome_raw($module, &$params, &$attribs)
{
	if (empty($module->content)){
		return false;
	}

	echo $module->content;
}

function modChrome_simple($module, &$params, &$attribs)
{
	if (empty($module->content)){
		return false;
	}

	$moduleClass = "module";
	$moduleClassSfx = $params->get('moduleclass_sfx');

	if($moduleClassSfx == "raw"){
		echo $module->content;
		return false;
	}

	if(!empty($moduleClassSfx)){
		$moduleClass .= " ".htmlspecialchars($moduleClassSfx);
	}
	?>
	<div class="<?= $moduleClass ?>">

		<?php if ($module->showtitle != 0): ?>
			<div class="module-title">
				 <?= $module->title ?>
			</div>
		<?php endif; ?>

		<?= $module->content ?>

	</div>
	<?
}

function modChrome_title_h2($module, &$params, &$attribs)
{
	if (empty($module->content)){
		return false;
	}

	$moduleClass = "module";
	$moduleClassSfx = $params->get('moduleclass_sfx');

	if($moduleClassSfx == "raw"){
		echo $module->content;
		return false;
	}

	if(!empty($moduleClassSfx)){
		$moduleClass .= " ".htmlspecialchars($moduleClassSfx);
	}
	?>
	<div class="<?= $moduleClass ?>">

		<?php if ($module->showtitle != 0): ?>
			<h2>
				 <?= $module->title ?>
			</h2>
		<?php endif; ?>

		<?= $module->content ?>

	</div>
	<?
}

function modChrome_aside($module, &$params, &$attribs)
{
	if (empty($module->content)){
		return false;
	}

	$moduleClass = "module";
	$moduleClassSfx = $params->get('moduleclass_sfx');

	if($moduleClassSfx == "raw"){
		echo $module->content;
		return false;
	}

	if(!empty($moduleClassSfx)){
		$moduleClass .= " ".htmlspecialchars($moduleClassSfx);
	}
	?>
	<div class="<?= $moduleClass ?>">

		<? if ($module->showtitle != 0): ?>
		<div class="aside-header">
			<?= $module->title ?>
		</div>
		<? endif; ?>

		<?= $module->content ?>

	</div>
	<?
}
function modChrome_aside_menu($module, &$params, &$attribs)
{
	if (empty($module->content)){
		return false;
	}

	$moduleClassSfx = $params->get('moduleclass_sfx');

	if($moduleClassSfx == "raw"){
		echo $module->content;
		return false;
	}
	?>
	<div class="aside-menu">

		<? if ($module->showtitle != 0): ?>
      <div class="aside-header aside-menu__title">
        <?= $module->title ?>
      </div>
		<? endif; ?>

		<?= $module->content ?>

	</div>
	<?
}

function modChrome_li($module, &$params, &$attribs)
{
	if (empty($module->content)){
		return false;
	}

	$moduleClass = "";
	$moduleClassSfx = $params->get('moduleclass_sfx');

	if($moduleClassSfx == "raw"){
		echo $module->content;
		return false;
	}

	if(!empty($moduleClassSfx)){
		$moduleClass = " class'".htmlspecialchars($moduleClassSfx)."'";
	}
	?>
	<li<?= $moduleClass ?>>
		<?= $module->content ?>
	</li>
	<?
}

function modChrome_footer_menu($module, &$params, &$attribs)
{
	if (empty($module->content)){
		return false;
	}

	$moduleClassSfx = $params->get('moduleclass_sfx');

	if($moduleClassSfx == "raw"){
		echo $module->content;
		return false;
	}

?>
  <div class="footer-menu">
    <? if ($module->showtitle != 0): ?>
    <div class="footer-header">
      <?= $module->title ?>
    </div>
    <? endif; ?>
    <?= $module->content ?>
  </div>
<?
}

function modChrome_footer_html($module, &$params, &$attribs)
{
	if (empty($module->content)){
		return false;
	}

	$moduleClassSfx = $params->get('moduleclass_sfx');

	if($moduleClassSfx == "raw"){
		echo $module->content;
		return false;
	}

?>
  <? if ($module->showtitle != 0): ?>
  <div class="footer-header">
    <?= $module->title ?>
  </div>
  <? endif; ?>
  <?= $module->content ?>
<?
}

function modChrome_xhtml_new($module, &$params, &$attribs)
{
	$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

	if($moduleclass_sfx == "raw"){
		echo $module->content;
		return false;
	}

	if (!empty ($module->content)) : ?>
		<div class="moduletable<?php if($moduleclass_sfx){echo $moduleclass_sfx;} ?>">
		<?php if ($module->showtitle != 0) : ?>
			<div class="module-title">
			    <?php echo $module->title; ?>
            </div>
		<?php endif; ?>
			<?php echo $module->content; ?>
		</div>
	<?php endif;
}

?>
