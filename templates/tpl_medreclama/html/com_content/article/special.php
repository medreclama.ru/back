<?
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
$dispatcher = JEventDispatcher::getInstance();
require JPATH_LIBRARIES."/simplehtmldom/simple_html_dom.php";

$html = new simple_html_dom();
$html->load($this->item->text);
$fourthParagraph = $html->find("p", 3); // четвертый параграф

if($fourthParagraph){
	$fourthParagraph->outertext .= " {loadmodule mod_custom,Есть реклама но мало пациентов}<br>";
	$this->item->text = $html;
}else{
	$this->item->text .= " {loadmodule mod_custom,Есть реклама но мало пациентов}<br>";
}

$this->item->text .= " {loadmodule mod_custom,Социальные сети}";
$this->item->text .= " {loadmodule mod_custom,У вас есть проект}";
$dispatcher->trigger('onContentPrepare', ['com_content.article', &$this->item, &$this->item->params, 0]);
?>
<div class="page-header">
    <h1><?= $this->escape($this->item->title) ?></h1>
</div>

<div class="blog-header">
    <div class="blog-header__item blog-header__item--date">
        опубликовано<br>
        <strong><?= JHtml::_('date', $this->item->publish_up, JText::_('DATE_FORMAT_LC4')) ?></strong>
    </div>
    <div class="blog-header__item blog-header__item--views">
        просмотров<br>
        <strong><?= $this->item->hits ?></strong>
    </div>
</div>

<div class="item-image">
	<? $images = json_decode($this->item->images) ?>
    <img
        src="<?= htmlspecialchars($images->image_fulltext) ?>"
        alt="<?= $this->escape($this->item->title) ?>"
        title="<?= $this->escape($this->item->title) ?>"
    >
</div>

<div class="grid grid--padding-y ">

    <div class="grid__cell grid__cell--m-9 grid__cell--12 blog-article">
		<?= $this->item->text ?>
		<?= $this->item->event->afterDisplayContent ?>
    </div>

    <div class="grid__cell grid__cell--m-3 grid__cell--12 ">
        <div class="blog-author">

            <? $authorSlug = JFilterOutput::stringURLSafe($this->item->author) ?>
            <? if(file_exists($_SERVER["DOCUMENT_ROOT"]."/images/users_profile_photo/".$authorSlug.".jpg")): ?>
            <div class="blog-author__photo">
                <img src="/images/users_profile_photo/<?= $authorSlug ?>.jpg"/>
            </div>
            <? endif ?>

            <div class="blog-author__name">
                Автор<br/>
                <a href="/o-kompanii">
                    <strong><?= $this->item->author ?></strong>
                </a>
                <br>
                Маркетолог, эксперт по продвижению клиник и руководитель агентства
            </div>

        </div>
    </div>

</div>





