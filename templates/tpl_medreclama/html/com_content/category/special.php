<?
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');

$dispatcher = JEventDispatcher::getInstance();

$this->category->text = $this->category->description;
$dispatcher->trigger('onContentPrepare', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$this->category->description = $this->category->text;

$results = $dispatcher->trigger('onContentAfterTitle', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayTitle = trim(implode("\n", $results));

$results = $dispatcher->trigger('onContentBeforeDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$beforeDisplayContent = trim(implode("\n", $results));

$results = $dispatcher->trigger('onContentAfterDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayContent = trim(implode("\n", $results));
?>
<div class="page-header">
    <h1>Блог</h1>
</div>
<div class="grid grid--padding-y  blog" itemscope itemtype="https://schema.org/Blog">

    <? if (!empty($this->lead_items)) : ?>
        <? foreach ($this->lead_items as &$item) : ?>
            <div
                class="grid__cell grid__cell--m-6 grid__cell--12 blog__item"
                itemprop="blogPost" itemscope itemtype="https://schema.org/BlogPosting"
            >
                <? $this->item = & $item ?>
                <?= $this->loadTemplate('item') ?>
            </div>
        <? endforeach ?>
    <? endif ?>

    <? if (($this->params->def('show_pagination', 1) == 1 || ($this->params->get('show_pagination') == 2)) && ($this->pagination->get('pages.total') > 1)) : ?>
        <div class="pagination grid__cell grid__cell--12">
            <? if ($this->params->def('show_pagination_results', 1)) : ?>
                <p class="counter pull-right"> <?= $this->pagination->getPagesCounter() ?> </p>
            <? endif ?>
            <?= $this->pagination->getPagesLinks() ?>
        </div>
    <? endif ?>

</div>

