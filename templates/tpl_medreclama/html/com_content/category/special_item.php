<? defined('_JEXEC') or die ?>
<? $itemUrl = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language)) ?>
<? $itemTitle = $this->escape($this->item->title) ?>
<? $images = json_decode($this->item->images) ?>
<h2 class="blog__title">
    <a href="<?= $itemUrl ?>" itemprop="url">
		<?= $itemTitle ?>
    </a>
</h2>

<a class="blog__image" href="<?= $itemUrl ?>">
    <img
        src="<?= htmlspecialchars($images->image_intro, ENT_COMPAT, 'UTF-8') ?>"
        alt="<?= $itemTitle ?>"
    />
</a>

<div class="blog__date">
    <?= JHtml::_('date', $this->item->publish_up, JText::_('DATE_FORMAT_LC1')) ?>
</div>




