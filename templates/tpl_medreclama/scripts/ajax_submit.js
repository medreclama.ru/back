function ajaxFormSubmit(params)
{
    var event = params['event'];
    var thisForm = $(event.target);
    var successRedirect = params['redirect'];
    var answerShowIn = params['answerShowIn'];
    var formReset = params['formReset'];

    thisForm.fadeTo(300, .3);

    var posting = $.post(thisForm.attr('action'), thisForm.serialize(), function(answer) {
        var message = getMessage(answer, params);
        message = formatMessage(message);

        if(answerShowIn == "popup"){
            $.fancybox.open(message);
        }

        if(answerShowIn == "replace"){
            thisForm.html(message);
        }
    });

    posting.done(function()
    {
        if(formReset){
            event.target.reset();
        }
        thisForm.fadeTo(300, 1);
    });

}

function getMessage(response, params)
{
    if(isSuccessMessageInResponse(response)){
        return getSuccessMessage(response, params);
    }else{
        return getErrorMessage(response, params);
    }
}

function formatMessage(message)
{
    return '<div class="center">' + message + '</div>';
}

function getErrorMessage(response, params)
{
    if(params["popupErrorTitle"] && params["popupErrorText"]){
        return params["popupErrorTitle"] + params["popupErrorText"];
    }

    if(isErrorMessageInResponse(response)){
        return getErrorMessageFromResponse(response);
    }

    return "<h3>Ошибка</h3>";
}

function getSuccessMessage(response, params)
{
    if(params["popupTitle"] && params["popupText"]){
        return params["popupTitle"] + params["popupText"];
    }else{
        return getSuccessMessageFromResponse(response);
    }
}

function isErrorMessageInResponse(response)
{
    return Boolean($('.rsform_error', $(response)).length);
}

function isSuccessMessageInResponse(response)
{
    return Boolean($('.rsform_success', $(response)).length);
}

function getErrorMessageFromResponse(response) {
    return $('.rsform_error', $(response)).html();
}

function getSuccessMessageFromResponse(response) {
    return $('.rsform_success', $(response)).html();
}

