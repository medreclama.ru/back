VK.Widgets.Like("vk_like2", {type: "button", pageUrl: window.location.href});

const toggleLock = (block, copy, state) => {
  if (state) {
    block.append(...(copy.childNodes));
  } else {
    copy.append(...(block.childNodes));
  }
}

VK.Auth.getLoginStatus((response) => {
  const lockedContent = document.querySelector('[data-social-lock]');
  const lockedContentCopy = document.createElement('div');
  let unlockState = false;
  if (response.session) {
    VK.Api.call('wall.get', {user_ids: '291780837', v:'5.73'}, res => {

      const posts = res.response.items

      posts.forEach((item) => {
        if (item.attachments) {
          item.attachments.forEach((att) => {
            if (att.type === 'link') {
              unlockState = att.link.url === window.location.href || unlockState === true ? true : false;
              console.log(att.link.url === window.location.href);
            }
          });
        }
      });
      toggleLock(lockedContent, lockedContentCopy, unlockState);
    });
    VK.Observer.subscribe("widgets.like.shared", () => {
      unlockState = true;
      toggleLock(lockedContent, lockedContentCopy, unlockState);
    });
  }
});
