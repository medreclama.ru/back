/***************** Functions *********************/
// set class "validated-form" to form to validate this form fields
// set class "validated" to field - to start validation of this field
// set class "validated-type-or" to field - to start validation of group fields with this class
// set class "required"  to field - set validation rule: field is required
// set class "min-chars_n"  to field - set validation rule: field should have at least n characters
// set class "max-chars_n"  to field - set validation rule: field should have no more then n characters
// set class "regex"  to field - set validation rule: field should pass regex validation

function formFieldsCheck(fields){

    var result = true;
    $.each(fields, function(index, field){
        if(!formFieldCheck($(field))){
            result = false;
        }
    });
    return result;
}

function formFieldsCheckTypeOr(fields, form){
    var result = false;
    $.each(fields, function(index, field){
        if(formFieldCheck($(field))){
            result = true;
            clearFieldsError(form);
            return false;
        }
    });
    return result;
}

function formFieldsCheckTypeRadio(fields, form){

    var result = true;
    $.each(fields, function(index, field){
        var inputRadio = $("input:radio:checked", $(field)).val();
        if(inputRadio == undefined){
            result = false;
            setFieldError($(field), 'required');
        }
    });
    return result;
}

function formFieldCheck(field)
{
    var result = true;

    if(field.hasClass('required')){
        result = formFieldCheckForRequired(field);
        if(!result){
            setFieldError(field, 'required');
            return false;
        }else{
            clearFieldError(field, 'required');
        }
    }

    if(field.hasClass('email')){
        result = formFieldCheckForEmail(field);
        if(!result){
            setFieldError(field, 'email');
            return false;
        }else{
            clearFieldError(field, 'email');
        }
    }

    if(field.hasClass('max-chars')){
        var maxChars = $(field).attr('data-max-chars');
        if(!maxChars){maxChars = 18;}
        result = formFieldCheckForMaxChars(field, maxChars);
        if(!result){
            setFieldError(field, 'max-chars');
            return false;
        }else{
            clearFieldError(field, 'max-chars');
        }
    }

    if(field.hasClass('min-chars')){
        var minChars = $(field).attr('data-min-chars');
        if(!minChars){minChars = 5;}
        result = formFieldCheckForMinChars(field, minChars);
        if(!result){
            setFieldError(field, 'min-chars');
            return false;
        }else{
            clearFieldError(field, 'min-chars');
        }
    }

    if(field.hasClass('regex')){
        var pattern = $(field).attr('data-regex');
        result = formFieldCheckForRegex(field, pattern);
        if(!result){
            setFieldError(field, 'regex');
            return false;
        }else{
            clearFieldError(field, 'regex');
        }
    }

    return true;
}

function formFieldCheckForRequired(field)
{
    var result = true;
    field = $(field);

    var type = field.prop("type");

    if(type == 'checkbox'){
        if(!field.prop('checked')){
            result = false;
        }
    }else{
        if(!field.val()){
            result = false;
        }
    }

    return result;
}

function formFieldCheckForEmail(field)
{
    field = $(field);
    var email = field.val();
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function formFieldCheckForMaxChars(field, maxChars)
{
    var result = true;
    field    = $(field);
    var fieldVal = field.val();

    if(fieldVal.length > maxChars){
        result = false;
    }

    return result;
}

function formFieldCheckForMinChars(field, minChars)
{
    var result = true;
    field = $(field);
    var fieldVal = field.val();

    if(fieldVal.length < minChars){
        result = false;
    }

    return result;
}

function formFieldCheckForRegex(field, pattern)
{
    var fieldVal = $(field).val();
    var regex = new RegExp(pattern, 'gi');
    if(fieldVal.match(regex) == null){
        return false;
    }

    return true;
}
function setFieldError(field, errorType){
    var fieldParent = $(field).parent();
    fieldParent.addClass('validation-error validation-error-' + errorType);

    if(field.hasClass('error-pulse')){
        fieldParent.addClass('error-show-pulse');
    }
    $('.validation-msg.validation-msg-required').text('Обязательное поле');
}

function clearFieldError(field, errorType){
    var fieldParent = $(field).parent();
    fieldParent.removeClass('validation-error validation-error-' + errorType);
}

function clearFieldsError(form){
    var removedClasses = 'validation-error';
    removedClasses += ' validation-error-required';
    removedClasses += ' validation-error-max-chars';
    removedClasses += ' validation-error-min-chars';
    removedClasses += ' validation-error-regex';
    $('.validation-error', form).removeClass(removedClasses);
}
/***************** /Functions *********************/

$(document).ready(function($)
{
    $('body').delegate('form.validated-form', 'submit', function(event)
    {
        var thisForm = $(this);

        var fieldsValidated = $('.validated', thisForm);
        var fieldsValidatedTypeOr = $('.validated-type-or', thisForm);
        var fieldsValidatedTypeRadio = $('.validated-type-radio', thisForm);

        clearFieldsError(thisForm);

        var validationResult = true;

        if(fieldsValidated.length){
            if(!formFieldsCheck(fieldsValidated)){
                validationResult = false;
            }
        }

        if(fieldsValidatedTypeOr.length){
            if(!formFieldsCheckTypeOr(fieldsValidatedTypeOr, thisForm)){
                validationResult = false;
            }
        }

        if(fieldsValidatedTypeRadio.length){
            if(!formFieldsCheckTypeRadio(fieldsValidatedTypeRadio, thisForm)){
                validationResult = false;
            }
        }

        if(!validationResult){
            event.preventDefault();
            return false;
        }

        // Если нет ошибок
        if(thisForm.hasClass('submitJS')){
            event.preventDefault();
            thisForm.trigger('submitJS');
        }

    });

    $('body').delegate('.validated', 'change', function(){
        var thisForm = $(this).parents('form');
        clearFieldsError(thisForm);
        formFieldCheck($(this));
    });
    $('body').delegate('.validated', 'keydown', function(){
        var thisForm = $(this).parents('form');
        clearFieldsError(thisForm);
    });
/*
    $('.validated-type-or').on('change', function(){
        formFieldsCheckTypeOr($(this));
    });
*/

});


