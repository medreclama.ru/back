$(document).ready(function()
{
    var blockThankYou = $('#bookPresentThankYou');
    var blockError = $('#bookPresentError');
    var title = $('title');

    if(blockThankYou.length)
    {
        var sendBook_name = Cookies.get('sendBook_name');
        var sendBook_email = Cookies.get('sendBook_email');

        if(
            sendBook_name != undefined
            && sendBook_email != undefined
        )
        {
            $.ajax(
            {
                url: '/getbook/index.php',
                data: 'name=' + sendBook_name + '&email=' + sendBook_email,
                type: "POST",
                cache: false,
                dataType: 'json',

                success: function(answer)
                {
                    if(answer == true){
                        blockThankYou.css("display", "flex");
                        yaCounter24832055.reachGoal('getbook');
                    }else{
                        getError(answer);
                    }
                },

                error: function(data)
                {
                    getError(data);
                }

            });
        }
        else
        {
            getError("sendBook_name OR sendBook_email undefined");
        }
    }

    function getError(data){
        blockError.css("display", "flex");
        title.html('Ошибка. Книга не была отправлена');
        console.log(data);
    }

});