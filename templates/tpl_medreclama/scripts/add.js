$(document).ready(function()
{
    if (window.location.pathname.startsWith('/blog/')) {
        var j = jQuery.noConflict();
        j('.modal').fancybox({});
    } else {
        $('.modal').fancybox({});
    }

    $('form.home-form__request').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            answerShowIn: "popup",
            formReset: true
        });
    });

    $('.content_component form.call_back').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            answerShowIn: "popup",
            formReset: true
        });
    });

    $('.content_component form.advert_offer').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            answerShowIn: "popup",
            formReset: true
        });
    });

    $('.popups form.call_back').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            answerShowIn: "replace",
            formReset: true
        });
    });

    $('.popups form.feedback').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            answerShowIn: "replace",
            formReset: true
        });
    });

    $('.popups form.audit_offer').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            answerShowIn: "replace",
            formReset: true
        });
    });

    $('.popups form.audit_prodayuschih_kachestv').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            answerShowIn: "replace",
            formReset: true
        });
    });

    $('.popups form.audit_kompleksnyi').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            answerShowIn: "replace",
            // formReset: true
        });
    });

    $('.popups form.konsultaciya_po_privlecheniyu_pacientov').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            answerShowIn: "replace",
            formReset: true
        });
    });

    $('form.form--case-offer').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            answerShowIn: "popup",
            formReset: true
        });
    });

	// Отслеживания конверсий на яндекс метрике
	$('form[name="med_adv"]').submit(function(){  // form_title = Реклама мед услуг
		yaCounter24832055.reachGoal('audit_besplatno');
	});

	$('form.advert_offer').submit(function(){  // form_title = Заявка на рекламу
		yaCounter24832055.reachGoal('kontakt_zayavka');
	});

	$('form[name="request_inside"]').submit(function(){  // form_title = Отправить заявку
		var href = document.location.href;

		if(href.indexOf('ob-agentstve') > 0){
        	yaCounter24832055.reachGoal('glav_zayavka');
		}
		if(href.indexOf('uslugi') > 0){
        	yaCounter24832055.reachGoal('uslugi_zayavka');
		}
		if(href.indexOf('mediynaya-reklama') > 0){
        	yaCounter24832055.reachGoal('media_zayavka');
		}
		if(href.indexOf('poiskovaya-optimizatsiya') > 0){
        	yaCounter24832055.reachGoal('seo_zayavka');
		}
		if(href.indexOf('kontekstnaya-reklama') > 0){
        	yaCounter24832055.reachGoal('kontekst_zayavka');
		}
		if(href.indexOf('reklama-v-sotsialnich-media') > 0){
        	yaCounter24832055.reachGoal('smm_zayavka');
		}
		if(href.indexOf('reports') > 0){
        	yaCounter24832055.reachGoal('otziv_zayavka');
		}

	});

	$('form.call_back').submit(function(){  // form_title = Запросить обратный звонок
		yaCounter24832055.reachGoal('obr_zvonok');
	});

	$('form[name="zakon_adv"]').submit(function(){  // form_title = Новый закон о рекламе
		yaCounter24832055.reachGoal('audit_zakon');
	});

  $('input[type="tel"], input.phone').on('focus', function(){
    $(this).maskfn('+7 (999) 999-99-99');
  });

});

$.extend({ // ф-ия создающая массив из параметров url
    getUrlVars: function(){
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function(name){
        return $.getUrlVars()[name];
    }
});

