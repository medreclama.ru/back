<? require __DIR__."/require_models.php";

$infoModel = new YoutubeApiSubtitleInfoModel;
$downloadModel = new DownloadModel;

if(!empty($_POST["request_info"])){
	try{
		$result = $infoModel->getInfo($_POST["video_url"]);
	} catch(Exception $e){
		$error = $e->getMessage();
	}
}

if(!empty($_POST["request_download"])){
	try{
		$infoModel->getResponseItems($_POST["video_url"]);
		$result = $downloadModel->downloadSubtitle($_POST["video_url"]);
	} catch(Exception $e){
		$error = $e->getMessage();
		if(empty($error) && !empty($e->xdebug_message)){
			$error = $e->xdebug_message;
		}
	}
}
