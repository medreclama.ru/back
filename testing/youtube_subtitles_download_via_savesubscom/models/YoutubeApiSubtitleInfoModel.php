<?
class YoutubeApiSubtitleInfoModel extends YoutubeApiModel
{
	public function getInfo($url)
	{
		$responseItems = $this->getResponseItems($url);
		return $this->renderInfo($responseItems);
	}

	public function getResponseItems($url)
	{
		if(empty($url)){
			throw new Exception('Поле "Youtube видео URL" не может быть пустым');
		}

		$videoId = $this->getVideoId($url);
		$requestUrl = $this->getInfoRequestUrl($videoId);
		$response = CurlModel::getContent($requestUrl);

		if(empty($response->items)){
			throw new Exception("Видео либо не найдено либо было удалено");
		}

		if(!$this->isVideoHasManualRussianSubtitle($response->items)){
			throw new Exception("Видео не содержит русских субтитров");
		}

		return $response->items;
	}

	private function getVideoId($url)
	{
		$explodeUrl = explode('?v=', $url);
		$videoId = end($explodeUrl);

		if(strpos($videoId, '&') !== false){
			$explodeVideoId = explode('&', $videoId);
			$videoId = reset($explodeVideoId);
		}

		return $videoId;
	}

	private function getInfoRequestUrl($videoId)
	{
		$requestUrl = "https://www.googleapis.com/youtube/v3/captions?";
		$requestUrl .= "videoId=".$videoId."&";
		$requestUrl .= "part=snippet&";
		$requestUrl .= "key=".self::API_KEY;

		return $requestUrl;
	}

	private function renderInfo($responseItems)
	{
		$content = "<table class='table' style='width: 80%'>";
		$content .= "<tr>";
		$content .= "<th>Subtitle Id</th>";
		$content .= "<th>Language</th>";
		$content .= "<th>Kind</th>";
		$content .= "</tr>";
		foreach ($responseItems as $item) {
			$content .= "<tr>";
			$content .= "<td>".$item->id."</td>";
			$content .= "<td>".$item->snippet->language."</td>";
			$content .= "<td>".$item->snippet->trackKind."</td>";
			$content .= "</tr>";
		}
		$content .= "</table>";

		return $content;
	}

	private function isVideoHasManualRussianSubtitle($responseItems)
	{
		foreach ($responseItems as $item) {
			if($item->snippet->language == "ru" && $item->snippet->trackKind == "standard"){
				return true;
			}
		}

		return false;
	}

}
