<?
class CurlModel
{
	public static function getContent($url, $postData = false)
	{
		if (!function_exists('curl_init')){
			die('CURL is not installed!');
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);

		if($postData){
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Host: savesubs.com',
				'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0',
				'Accept: application/json, text/plain, */*',
				'Accept-Encoding: gzip, deflate, br',
				'Content-Type: application/json; charset=UTF-8',
				'Origin: https://savesubs.com',
				'Connection: keep-alive',
				'Pragma: no-cache',
				'Cache-Control: no-cache',
				'TE: Trailers'
			));

		}

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output);

		if(!empty($response->error)){
			if(!empty($response->error->message)){
				$errorMessage = $response->error->message;
			}else{
				$errorMessage = "Ошибка во время запроса к ".$url;
			}
			throw new Exception($errorMessage);
		}

		return $response;
	}

	public static function get_web_page($url, $postData)
	{
		$user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

		$options = array(
			CURLOPT_CUSTOMREQUEST  => "POST",        //set request type post or get
			CURLOPT_POST           => true,        //set to GET
			CURLOPT_POSTFIELDS     => $postData,
			CURLOPT_USERAGENT      => $user_agent, //set user agent
			CURLOPT_COOKIEFILE     => "cookie.txt", //set cookie file
			CURLOPT_COOKIEJAR      => "cookie.txt", //set cookie jar
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 10,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => false,
		);

		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		$content = iconv("windows-1251", "UTF-8", $content);

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;

		var_dump($header);

		return $header;
	}

}