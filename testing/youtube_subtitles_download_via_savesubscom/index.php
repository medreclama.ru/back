<? error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED) ?>
<? $title = "8124 - Скрипт парсинга и обработки субтитров (download)" ?>
<? require __DIR__."/controller.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?= $title ?></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>

	<div class="container">

        <br><h3><?= $title ?></h3><br>

		<form action="<?= $_SERVER["PHP_SELF"] ?>" method="post" >

            <div class="form-row">
                <div class="form-group col-md-5">
                    <input
                        type="text"
                        name="video_url"
                        class="form-control"
                        placeholder="Youtube видео URL"
                        value="<?= !empty($_POST["video_url"]) ? $_POST["video_url"] : "" ?>"
                        required="required"
                    >
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-2">
                    <input type="submit" name="request_info" value="Инфо о субтитрах" class="btn btn-info">
                </div>

                <div class="form-group col-md-2">
                    <input type="submit" name="request_download" value="Загрузить субтитры" class="btn btn-primary">
                </div>
            </div>

		</form>

        <br>
		<? if(!empty($result)): ?>
            <div class="alert alert-success">
                <h4 class="alert-heading">Ok!</h4>
            </div>
			<?= $result?>
        <? endif ?>

		<? if(!empty($error)): ?>
            <div class="alert alert-danger">
                <h4 class="alert-heading">Fail!</h4>
            </div>
			<?= $error ?>
		<? endif ?>

	</div>

</body>
</html>
