<?
use Codeception\Test\Unit;
use Models\Parser\AbstractParser;
use Models\Parser\SimpleHtmlDom\SimpleHtmlDomModel;
use Models\RemoteReader;
use Models\TextHelper;

class MainTest extends Unit
{
    /** @var \UnitTester */
    protected $tester;


    public function testMedmarketingUaParser()
    {
		$url = "https://medmarketing.ua/blog/zarabotok-v-instagram-4-sposoba-zarabotka-v-instagram/";
//		$this->tester->assertEquals(18941, $this->getArticleLength($url));
    }

    public function testMedanalyticsRuParser()
    {
		$url = "https://medanalytics.ru/blog/izmenenie-srosa-na-meditsinskie-uslugiv-period-koronavirusa/";
//		$this->tester->assertEquals(4800, $this->getArticleLength($url));
    }

    public function testSymmetriaMedRuParser()
    {
		$url = "http://symmetria-med.ru/blog/onlayn-konferentsii-polnaya-klinika-2021.html";
//		$this->tester->assertEquals(463, $this->getArticleLength($url));
    }

    public function testRoimedRuParser()
    {
		$url = "http://roimed.ru/posts/93-roskomnadzor-poluchil-pravo-shtrafovat-nedobrosovestnyh-operatorov-personalnyh-dannyh.html";
//		$this->tester->assertEquals(1941, $this->getArticleLength($url));
    }

    public function testReclientRuParser()
    {
		$url = "http://reclient.ru/analytics/829/";
		$this->tester->assertEquals(9599, $this->getArticleLength($url));
    }

	private function getArticleLength($url)
	{
		$remoteReader = new RemoteReader;
		$content = $remoteReader->getContent($url);

		$simpleHtmlDomModel = new SimpleHtmlDomModel($content);

		$parser = AbstractParser::getParser($url, $simpleHtmlDomModel);

		$articleText = $parser->getArticleText();
		$textLength = TextHelper::getTextLength($articleText);

		return $textLength;
    }


}