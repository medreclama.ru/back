<?php
$_SERVER["DOCUMENT_ROOT"] = str_replace('\testing\get_article_text_length\App\Tests\unit', "", __DIR__);

$pathModels = __DIR__."/../../Models";
require_once $pathModels."/Parser/AbstractParser.php";

$dir = new RecursiveDirectoryIterator($pathModels);
foreach (new RecursiveIteratorIterator($dir) as $file) {
	if (!is_dir($file)) {
		if( fnmatch('*.php', $file) ){
			require_once $file;
		}
	}
}