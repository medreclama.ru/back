<?
namespace Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use System\BaseController;
use System\Helper;

use Models\RemoteReader;
use Models\Parser\AbstractParser;
use Models\Parser\SimpleHtmlDom\SimpleHtmlDomModel;
use Models\TextHelper;

class Controller extends BaseController
{
    public function getLength(Request $request, Response $response)
    {
		$body = $response->getBody();
    	try{
			$textLength = $this->getLengthPrivate($response);
			$body->write($textLength);
		}catch (\Exception $e){
    		$message = $e->getMessage();
			$body->write($message);
		}
    }

	private function getLengthPrivate(Response $response): int
	{
		if(empty($_POST["url"])){
			throw new \Exception("Обработчику не был передан URL статьи");
		}

		$url = $_POST["url"];

		$remoteReader = new RemoteReader;
		$content = $remoteReader->getContent($url);

		$simpleHtmlDomModel = new SimpleHtmlDomModel($content);

		$parser = AbstractParser::getParser($url, $simpleHtmlDomModel);

		$articleText = $parser->getArticleText();
		$textLength = TextHelper::getTextLength($articleText);

		return $textLength;
    }

}
