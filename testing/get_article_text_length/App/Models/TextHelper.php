<?
namespace Models;

class TextHelper
{
	public static function getTextLength(string $text): int
	{
		return mb_strlen($text, "utf-8");
	}

}