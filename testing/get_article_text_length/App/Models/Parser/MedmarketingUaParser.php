<?
namespace Models\Parser;

class MedmarketingUaParser extends AbstractParser
{
	public function getArticleText(): string
	{
		$textObj = $this->simpleHtmlDom->find(".detail .content", 0);

		if(!$textObj){
			throw new \Exception("Не найден текст статьи по правилам обработчика");
		}

		$text = $textObj->plaintext;

		$text = self::trimSpaces($text);

		return $text;
	}

}