<?
namespace Models\Parser;

class MedanalyticsRuParser extends AbstractParser
{
	public function getArticleText(): string
	{
		$text = "";
		$messageNotFoundText = "Не найден текст статьи по правилам обработчика";

		$strings = $this->simpleHtmlDom->find(".content_small");
		if(!$strings){
			throw new \Exception($messageNotFoundText);
		}

		foreach ($strings as $string){
			if($string->class == "content_small fw"){
				continue;
			}

			$string->removeChild($string->find("div#breadcrumb", 0));
			$text .= $string->plaintext;
		}

		if(!$text){
			throw new \Exception($messageNotFoundText);
		}

		$text = self::trimSpaces($text);

		return $text;
	}

}