<?
namespace Models\Parser;

use Models\Parser\SimpleHtmlDom\SimpleHtmlDomModel;

abstract class AbstractParser
{
	protected $simpleHtmlDom;

	public function __construct(SimpleHtmlDomModel $simpleHtmlDomModel)
	{
		$this->simpleHtmlDom = $simpleHtmlDomModel;
	}

	public function getArticleText(): string {return "";}

	public static function getParser(string $url, SimpleHtmlDomModel $simpleHtmlDomModel): self
	{
		if(strpos($url, "medmarketing.ua") !== false){
			return new MedmarketingUaParser($simpleHtmlDomModel);
		}

		if(strpos($url, "medanalytics.ru") !== false){
			return new MedanalyticsRuParser($simpleHtmlDomModel);
		}

		if(strpos($url, "symmetria-med.ru") !== false){
			return new SymmetriaMedRuParser($simpleHtmlDomModel);
		}

		if(strpos($url, "roimed.ru") !== false){
			return new RoimedRuParser($simpleHtmlDomModel);
		}

		if(strpos($url, "reclient.ru") !== false){
			return new ReclientRuParser($simpleHtmlDomModel);
		}

		throw new \Exception("На найден обработчик для этого сайта");
	}

	protected static function trimSpaces(string $text): string
	{
		$text = str_replace("&nbsp;", " ", $text);
		$text = preg_replace(['/\s{2,}/', '/[\t\n]/'], ' ', $text);
		$text = trim($text);

		return $text;
	}

}