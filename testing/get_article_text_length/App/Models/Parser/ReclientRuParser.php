<?
namespace Models\Parser;

class ReclientRuParser extends AbstractParser
{
	public function getArticleText(): string
	{
		$textObj = $this->simpleHtmlDom->find("#content", 0);

		if(!$textObj){
			throw new \Exception("Не найден текст статьи по правилам обработчика");
		}

		$textObj->removeChild($textObj->find(".author_block", 0));
		$textObj->removeChild($textObj->find(".similar_posts", 0));
		$textObj->removeChild($textObj->find(".reviews-collapse", 0));
		$textObj->removeChild($textObj->find(".call-center-button", 0));
		$textObj->removeChild($textObj->find(".reviews-reply-form", 0));
		$textObj->removeChild($textObj->find(".detail_sidebar-blocks", 0));

		$text = $textObj->plaintext;

		$text = str_replace("&lt;Вернуться в Журнал", "", $text);

		$text = self::trimSpaces($text);

		return $text;
	}

}