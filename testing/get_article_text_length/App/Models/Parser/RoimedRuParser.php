<?
namespace Models\Parser;

class RoimedRuParser extends AbstractParser
{
	public function getArticleText(): string
	{
		$textObj = $this->simpleHtmlDom->find(".content_item .f_content", 0);

		if(!$textObj){
			throw new \Exception("Не найден текст статьи по правилам обработчика");
		}

		$text = $textObj->plaintext;

		$resumeTextObj = $this->simpleHtmlDom->find(".content_item .f_resume", 0);
		if($resumeTextObj){
			$text .= $resumeTextObj->plaintext;
		}

		$text = self::trimSpaces($text);

		return $text;
	}

}