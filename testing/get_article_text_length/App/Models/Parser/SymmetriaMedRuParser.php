<?
namespace Models\Parser;

class SymmetriaMedRuParser extends AbstractParser
{
	public function getArticleText(): string
	{
		$textObj = $this->simpleHtmlDom->find(".l-blogSingle-description", 0);

		if(!$textObj){
			throw new \Exception("Не найден текст статьи по правилам обработчика");
		}

		$text = $textObj->plaintext;

		$text = self::trimSpaces($text);

		return $text;
	}

}