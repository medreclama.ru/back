<?
namespace Models;

class RemoteReader
{
    public function getContent(string $url): string
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $content = curl_exec($ch);

        if (curl_errno($ch)) {
            curl_close($ch);
			throw new \Exception("Ошибка во время проверки доступности URL");
        }

        $http_code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        curl_close($ch);

        if($http_code !== 200){
			throw new \Exception("Статус ответа по URL страницы не равен 200");
        }

        return $content;
    }

}