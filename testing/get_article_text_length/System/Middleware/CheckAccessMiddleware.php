<?
namespace Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Exceptions\AbortException;

class CheckAccessMiddleware
{
    private $allowedLogins = [
        "rt-webdesign",
        "dmitriy.m",
    ];

    public function __invoke(Request $request, Response $response, callable $next)
    {
        global $USER;

        if (!@$USER->IsAuthorized()){
            throw new AbortException("Вы не авторизованны");
        }

        $userLogin = $USER->GetLogin();

        if(!in_array($userLogin, $this->allowedLogins)){
            throw new AbortException("Вам не разрешен доступ к скрипту");
        }

        $response = $next($request, $response);

        return $response;
    }

}