<?
namespace Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class IncludeJoomlaCoreMiddleware
{
    public function __invoke(Request $slimRequest, Response $slimResponse, callable $next)
    {
		require __DIR__."/../../System/joomlaCore.php";
        $slimResponse = $next($slimRequest, $slimResponse);

        return $slimResponse;
    }

}