<?php
require_once "includes/simple_html_dom.php";
ini_set("display_errors", 1);
error_reporting(E_ALL);

class HL_Tester
{
	public $siteName = "hairlife.ru";
	private $siteUrl = "http://www.hairlife.ru";
	private $log = "log.txt";
	private $emails = [
		"rt-webdesign@mail.ru",
		"zarya@medreclama.ru"
	];

	public function is200Response()
	{
		$headers = get_headers($this->siteUrl);
		$response = $headers[0];
		if(strpos($response, "200 OK") !== false){
			return true;
		}
		throw new Exception("Сайт не вернул код 200");
	}

	public function isContentOnPage()
	{
		$html = file_get_html($this->siteUrl);
		if($html->find("title")){
			return true;
		}
		$html = substr($html->outertext, 0, 600);
		$message  = "Сайт $this->siteName отвечает, но на страницах нет контента\n\n";
		$message .= "Текст страницы: \n".$html;
		throw new Exception($message);
	}

	public function logSave($status)
	{
		file_put_contents($this->log, $status);
	}


	public function getLogStatus()
	{
		return file_get_contents($this->log);
	}

	public function sendNotification($message)
	{
		if($_SERVER['REMOTE_ADDR'] == "127.0.0.1") {return false;}

		$message .= "\n\nДата/время: ".date('Y.m.d H:i:s');

		$subject = "Диагностическое сообщение с сайта $this->siteName";

		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/plain; charset=utf-8\r\n";

		foreach($this->emails as $email){
			mail($email, $subject, $message, $headers);
		}
		return true;
	}

}

$tester = new HL_Tester;
$logStatus = $tester->getLogStatus();

try{
	$tester->is200Response();
	$tester->isContentOnPage();

	if($logStatus == "off"){
		$tester->sendNotification("Сайт $tester->siteName снова работает нормально");
	}

	$tester->logSave("on");
}
catch(Exception $e)
{
	// Отправляем уведомление только если это повторная ошибка
	if($logStatus == "pending"){
		$tester->logSave("off");
		$message = "Ошибка в работе сайта $tester->siteName: ".$e->getMessage();
		$tester->sendNotification($message);
	}elseif($logStatus == "on"){
		$tester->logSave("pending");
	}

}