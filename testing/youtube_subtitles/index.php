<?php
/**
 * Sample PHP code for youtube.captions.download
 * See instructions for running these code samples locally:
 * https://developers.google.com/explorer-help/guides/code_samples#php
 *
 * Also note that this sample code downloads a file and can't be executed
 * via this interface. To test this sample, you must run it locally using your
 * own API credentials.
 */

if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
	throw new Exception(sprintf('Please run "composer require google/apiclient:~2.0" in "%s"', __DIR__));
}
require_once __DIR__ . '/vendor/autoload.php';

$client = new Google_Client();
$client->setApplicationName('API code samples');
$client->setScopes([
	'https://www.googleapis.com/auth/youtube.force-ssl',
]);

error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

// TODO: For this request to work, you must replace
//       "YOUR_CLIENT_SECRET_FILE.json" with a pointer to your
//       client_secret.json file. For more information, see
//       https://cloud.google.com/iam/docs/creating-managing-service-account-keys
$client->setAuthConfig('client_secret.json');
$client->setAccessType('offline');
$client->setRedirectUri('https://medreclama.ru/testing/youtube_subtitles/redirect.php');

// Request authorization from the user.
$authUrl = $client->createAuthUrl();
printf("Open this link in your browser:\n%s\n", $authUrl);
print('Enter verification code: ');
$authCode = trim(fgets(STDIN));

// Exchange authorization code for an access token.
$accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
$client->setAccessToken($accessToken);

// Get the authorized Guzzle HTTP client.
$http = $client->authorize();

/**
 * Open the file where the downloaded content will be written.
 * TODO: For this request to work, you must replace "YOUR_FILE"
 * with the path to the file where that content should be written.
 */
$fp = fopen('file.txt', 'w');

/**
 * The URL path for this request is:
 *     /youtube/v3/youtube/v3/captions/{id}
 * In the path, the string "{id}" is a placeholder
 * for the value of the corresponding request parameter.
 */
$response = $http->request(
	'GET',
	'/youtube/v3/youtube/v3/captions/otHjcM26CtP6qzvrMf-NtrqJpCmG3FId'
);
fwrite($fp, $response->getBody()->getContents());
fclose($fp);

die("!!!");

