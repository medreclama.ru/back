<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
	throw new Exception(sprintf('Please run "composer require google/apiclient:~2.0" in "%s"', __DIR__));
}
require_once __DIR__ . '/vendor/autoload.php';


$client = new Google_Client();
$client->setApplicationName('API code samples');
$client->setScopes(['https://www.googleapis.com/auth/youtube.force-ssl']);

$client->setAuthConfig('client_secret.json');
$client->setAccessType('offline');
$client->setRedirectUri('https://medreclama.ru/testing/youtube_subtitles/redirect.php');


// Exchange authorization code for an access token.
$accessToken = $client->fetchAccessTokenWithAuthCode($_GET["code"]);
$client->setAccessToken($accessToken);


// Get the authorized Guzzle HTTP client.
$http = $client->authorize();

/**
 * Open the file where the downloaded content will be written.
 * TODO: For this request to work, you must replace "YOUR_FILE"
 * with the path to the file where that content should be written.
 */
$fp = fopen('file.txt', 'w');

/**
 * The URL path for this request is:
 *     /youtube/v3/youtube/v3/captions/{id}
 * In the path, the string "{id}" is a placeholder
 * for the value of the corresponding request parameter.
 */

$response = $http->request(
	'GET',
	'/youtube/v3/captions/otHjcM26CtP6qzvrMf-NtrqJpCmG3FId?key=AIzaSyAGTVAP9h120wXEnZM_qlEOSVoWMBzMmpI'
);

if(!empty($response->error)){
	echo $response->error->message;
}

fwrite($fp, $response->getBody()->getContents());
fclose($fp);





