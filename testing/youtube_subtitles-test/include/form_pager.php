<? if($prevPageToken || $nextPageToken): ?>
    <br>
	<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post" class="form-inline">

        <div class="pager_wrapper">
            <? if($prevPageToken): ?>
                <input type="hidden" name="page_token_prev" value="<?= $prevPageToken ?>">
                <input type="submit" class="btn btn-primary" value="<-- Предыдущая страница результатов" name="get_videos_prev">
            <? endif ?>

            <? if($nextPageToken): ?>
                <input type="hidden" name="page_token_next" value="<?= $nextPageToken ?>">
                <input type="submit" class="btn btn-primary" value="Следующая страница результатов -->" name="get_videos_next">
            <? endif ?>
        </div>

        <input type="hidden" name="search_request" value="<?= $searchRequest ?>">
		<input type="hidden" name="thumbnails" value="<?= $thumbnails ?>">
		<input type="hidden" name="videos_per_page" value="<?= $videosPerPage ?>">
		<input type="hidden" name="min_chars" value="<?= $minChars ?>">

	</form>
<? endif ?>