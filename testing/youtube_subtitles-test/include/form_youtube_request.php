<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">

	<div class="form-row">

		<div class="form-group col-md-5">
			<label for="search_request">Запрос</label>
			<input
				type="text"
				class="form-control"
				name="search_request"
				id="search_request"
				placeholder="пример: стрижка мастер класс"
				value="<?= !empty($_POST["search_request"]) ? htmlspecialchars($_POST["search_request"]) : "" ?>"
				size="75"
				required
			>
		</div>

		<div class="form-group col-md-3">
			<label for="min_chars">Мин. количество символов</label>
			<input
				type="text"
				class="form-control"
				name="min_chars"
				id="min_chars"
				placeholder=""
				value="<?= !empty($_POST["min_chars"]) ? $_POST["min_chars"] : "" ?>"
				size="3"
				pattern="\d+"
			>
		</div>

	</div>

	<div class="form-row">

		<div class="form-group col-md-5">
			<label>Превью: </label><br>

			<div class="form-check form-check-inline">
				<input class="form-check-input" type="radio" name="thumbnails" id="thumbnails1" value="default" <?= ($thumbnails == "default") ? "checked": "" ?>>
				<label class="form-check-label" for="thumbnails1">Стандртное</label>
			</div>

			<div class="form-check form-check-inline">
				<input class="form-check-input" type="radio" name="thumbnails" id="thumbnails2" value="medium" <?= ($thumbnails == "medium") ? "checked": "" ?>>
				<label class="form-check-label" for="thumbnails2">Среднее</label>
			</div>

			<div class="form-check form-check-inline">
				<input class="form-check-input" type="radio" name="thumbnails" id="thumbnails3" value="high" <?= ($thumbnails == "high") ? "checked": "" ?>>
				<label class="form-check-label" for="thumbnails3">Большое</label>
			</div>

		</div>

		<div class="form-group col-md-3">
			<label for="min_chars">Количество видео на странице</label>
			<input
				type="text"
				class="form-control"
				name="videos_per_page"
				id="videos_per_page"
				placeholder=""
				value="<?= !empty($_POST["videos_per_page"]) ? $_POST["videos_per_page"] : "" ?>"
				size="3"
				pattern="\d+"
			>
		</div>
	</div>

	<input type="submit" class="btn btn-primary" value="Поиск" name="get_videos">

</form>
