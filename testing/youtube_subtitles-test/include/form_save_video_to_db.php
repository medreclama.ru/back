<? /** @var $video VideoModel */ ?>
<form
	action="<?= $_SERVER['PHP_SELF'] ?>"
	method="get"
	class="form-inline <?= $isStatusPended ? "save_success" : "" ?>"
	name="saveToDb"
>

	<div class="form-group">
		<select name="status" id="status" class="form-control mb-2 mr-sm-2 form-control-sm">
			<option value="">- Выберите статус -</option>
			<option value="1">плохое</option>
			<option value="2" <?= $isStatusPended ? "selected" : "" ?>>на потом</option>
			<option value="3" >в работу</option>
		</select>
	</div>

	<div class="form-group">
		<input
			type="text"
			name="search_request"
			class="form-control mb-2 mr-sm-2 form-control-sm"
			id="searchRequest"
			value=""
			size="100"
            placeholder="Основной запрос"
		>
	</div>

	<div class="form-group">
		<input type="submit" value="Сохранить" class="btn btn-info mb-2 mr-sm-2 btn-sm">
	</div>

    <input type="hidden" name="id" value="<?= $video->id ?>">
    <input type="hidden" name="title" value="<?= htmlspecialchars($video->title) ?>">
    <input type="hidden" name="description" value="<?= htmlspecialchars($video->description) ?>">
    <input type="hidden" name="subtitle" value="<?= htmlspecialchars($video->subtitle) ?>">

</form><br>