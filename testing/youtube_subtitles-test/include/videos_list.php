<? /** @var $video VideoModel */ ?>
<? foreach ($videos as $video): ?>
<div class="blockVideo">

    <? $isStatusPended = VideoModel::isStatusPended($video->dbStatusId) ?>

	<div class="videoTitle">
		<a
            href="https://www.youtube.com/watch?v=<?= $video->id ?>"
            target="_blank"
        ><?= $video->title ?></a>
	</div>

	<div class="videoFull">

        <? if(!empty($video->subtitle)): ?>
		<div class="videoFullLeft">
			<img src="<?= $video->thumbnail ?>">
		</div>

		<div class="videoFullRight">
			<? if(!empty($video->description)): ?>
				<div class="videoDesc">
					<strong>Описание: </strong><?= $video->description ?>
				</div><br>
            <? endif ?>

			<div class="videoSubtitle">

				<p><strong>Субтитры: </strong></p>
                
                <? require __DIR__."/form_save_video_to_db.php" ?>

                <div class="videoSubtitleContent">
                    <?= $video->subtitle ?>
                </div>

			</div>
		</div>
        <? else: ?>
            <div class="alert alert-danger"><?= $video->message ?></div>
        <? endif ?>
	</div>
</div>
<? endforeach ?>