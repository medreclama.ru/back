jQuery(document).ready(function($)
{
    var maxHeight = 300;
    var subtitleBlocks = $(".videoSubtitle");

    subtitleBlocks.each(function () {
        var subtitleHeight = $(this).height();
        if(subtitleHeight > maxHeight){
            $(this).addClass("videoSubtitleClip");
            $(this).after("<button class='more' title='показать полностью'></button>");
        }
    });

    $(".result").delegate(".more", "click", function (e) {
        $(this).siblings().removeClass("videoSubtitleClip");
        $(this).detach();
    });

    var saveToDbForms = $("form[name='saveToDb']");
    saveToDbForms.on("submit", function (e)
    {
        e.preventDefault();
        var form = $(this);
        var data = form.serialize();
        $.post(
            form.attr("action") + "?saveToDb=1",
            data,
            function(answer){
                if(answer.status){
                    form.addClass("save_success");
                    form.removeClass("save_error");
                    $(".alert", form).detach();
                }else{
                    var message = "<div class='alert alert-danger'>" + answer.message + "</div>";
                    form.addClass("save_error");
                    form.append(message);
                    form.removeClass("save_success");
                }
            },
            "json"
        );
    });

});

