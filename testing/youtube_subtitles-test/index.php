<? ini_set("display_errors", 1) ?>
<? error_reporting(E_ALL & ~E_STRICT & ~E_DEPRECATED) ?>
<? $title = "8124 - Скрипт парсинга и обработки субтитров" ?>
<? require __DIR__."/controller.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?= $title ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

	<div class="container">

        <br><h3><?= $title ?></h3><br>

        <? require __DIR__."/include/form_youtube_request.php" ?>

		<? if($videos): ?>
            <div class="result">
                <? require __DIR__."/include/videos_list.php" ?>
                <? require __DIR__."/include/form_pager.php" ?>
            </div>
        <? endif ?>

		<? if(!empty($error)): ?>
            <br>
            <div class="alert alert-danger">
                <h5 class="alert-heading">Ошибка!</h5>
            </div>
			<?= $error ?>
		<? endif ?>

	</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/script.js"></script>

</body>
</html>
