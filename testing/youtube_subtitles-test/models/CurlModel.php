<?
class CurlModel
{
	public static function getContent($url, $method = "get")
	{
		if (!function_exists('curl_init')){
			die('CURL is not installed!');
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);

		if($method == "post"){
			curl_setopt($ch, CURLOPT_POST, 1);
		}

		$userAgent = "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0";
		curl_setopt($ch, CURLOPT_HTTPHEADER, [$userAgent]);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$output = curl_exec($ch);
		curl_close($ch);

		if(self::isJson($output)){
			$response = json_decode($output);

			if(!empty($response->error)){
				if(!empty($response->error->message)){
					$errorMessage = $response->error->message;
				}else{
					$errorMessage = "Ошибка во время запроса к ".$url;
				}
				throw new Exception($errorMessage);
			}
		}else{
			$response = $output;
		}

		return $response;
	}

	private static function isJson($string)
	{
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

}