<?
class Helper
{
	public static function d($items, $isReturn = false, $isPrintr = false)
	{
		if($isReturn){ob_start();}

		echo '<pre>';

		if($isPrintr){
			print_r($items);
		}else{
			var_dump($items);
		}

		echo '</pre>';

		if($isReturn){
			$result = ob_get_clean();
			return $result;
		}
	}

	public static function dd($items, $e)
	{
	    echo '<pre>';
        var_dump($items);
        echo '</pre>';
        self::dieNow($e);
	}

	public static function dieNow($e)
	{
		die('<h2>Exit!</h2><b>File: </b>'.$e->getFile().'<br><b> Line: </b>'.$e->getLine());
	}

	/**
	* Generate message varius of type
	* @param string $text Text to display
	* @param string $type Type of message, can be 'success', 'info', 'warning', 'danger'
	* @return string Returns text of message.
	*/
	public static function message($text, $type = 'success', $return = false)
	{
		$pattern    = '<div class="alert alert-#TYPE#">#TEXT#</div>';
		$arrSearch  = array('#TYPE#', '#TEXT#');
		$arrReplace = array($type, $text);
		$result  = str_replace($arrSearch, $arrReplace, $pattern);
		$result = self::beautifyMessage($result);

		if($return){
			return $result;
		}

		echo $result;
	}

	public static function msgInfo($text)   {self::message($text, "info");}
	public static function msgSucces($text) {self::message($text, "success");}
	public static function msgWarning($text){self::message($text, "warning");}
	public static function msgDanger($text) {self::message($text, "danger");}

	public static function panel($text, $title, $type = "info", $isReturn = false)
	{
		$pattern    = '<div class="panel panel-#TYPE#"><div class="panel-heading"><h3 class="panel-title">#TITLE#</h3></div><div class="panel-body">#TEXT#</div></div>';
		$arrSearch  = array('#TYPE#', '#TEXT#', '#TITLE#');

		if(is_array($text)){
			$text = self::d($text, true);
		}

		$arrReplace = array($type, $text, $title);
		$arrSearch2  = array('success!', 'info!', 'primary!', 'warning!', 'danger!');
		$arrReplace2 = array('Success!', 'Information!', 'Information!', 'Warning!', 'Error!');
		$result  = str_replace($arrSearch, $arrReplace, $pattern);
		$result  = str_replace($arrSearch2, $arrReplace2, $result);

		$result = preg_replace("/<script.*?\/script>/s", '', $result);
		$result = preg_replace("/<object.*?\/object>/s", '', $result);

		$result = self::beautifyMessage($result);

		if($isReturn){
			return $result;
		}

		echo $result;
	}

	public static function beautifyMessage($text)
	{
		$patternBold = '/\|(.*?)\|/';
		$replacementBold = '<b>#TEXT#</b>';
		$text	= self::beautifyMessageByType($patternBold, $replacementBold, $text);

		$patternBadge = '/\%(.*)\%/';
		$replacementBadge = '<span class="badge">#TEXT#</span>';
		$text	= self::beautifyMessageByType($patternBadge, $replacementBadge, $text);

		return $text;
	}

	private static function beautifyMessageByType($pattern, $replacement, $text)
	{
		preg_match_all($pattern, $text, $matches);
		if($matches[0]){
			foreach($matches[0] as $key => $match){
				$text = str_replace($match, $replacement, $text);
				$text = str_replace('#TEXT#', $matches[1][$key], $text);
			}
		}

		return $text;
	}

	public static function table($array, $escape = false, $isReturn = false, $tableId = "")
	{
		// start table
		if($tableId){
			$html = '<table id="'.$tableId.'">';
		}else{
			$html = '<table>';
		}

		// header row
		$html .= '<tr>';
		$firstRow = reset($array);
		foreach($firstRow as $key=>$value){
			if($escape){
				$html .= '<th>' . htmlspecialchars($key) . '</th>';
			}else{
				$html .= '<th>' . $key . '</th>';
			}

		}
		$html .= '</tr>';

		// data rows
		foreach( $array as $key=>$value){
			$html .= '<tr>';
			foreach($value as $key2=>$value2){
				if($escape){
					$html .= '<td>' . htmlspecialchars($value2) . '</td>';
				}else{
					$html .= '<td>' . $value2 . '</td>';
				}
			}
			$html .= '</tr>';
		}

		// finish table and return it

		$html .= '</table>';

		if(!$isReturn){
			echo $html;
			return true;
		}

		return $html;
	}

	public static function renderControllers($appFullUrl, $controllers, $title = "")
	{
		$lis = "";
		foreach($controllers as $controller){
			$cLink = $appFullUrl.$controller[0];

			if(!empty($controller[1])){
			    $cTitle = $controller[1];
			}

			$cType = "info";
			if(!empty($controller[2])){
			   $cType = $controller[2];
			}
			if($cLink == "divider"){
				$lis .= "<br><br>\n";
			}else{
				$lis .= '<li><a href="'.$cLink.'" class="btn btn-'.$cType.'">'.$cTitle.'</a></li>'."\n";
			}
		}
		$result = '<ul class="list-inline">'.$lis.'</ul><br>';
		if(!empty($title)){
			$result = "<h4>$title</h4>\n".$result."\n\n";
		}
		echo $result;
	}

	public static function link($url, $anchor = "", $class = "", $target = "")
	{
		if($class)  {$class = " class='$class'";}
		if(!$target){$target = "_blank";}
		if(!$anchor){$anchor = $url;}

		$result = "<a href='".$url."' target='".$target."'.$class>".$anchor."</a>";
		return $result;
	}

	public static function listUrls($arUrls, $target = "")
	{
		$result = "";
		foreach($arUrls as $url){
			$result .= "<li>".self::link($url, "", "", $target)."</li>";
		}
		return "<ul>$result</ul>";
	}

	public static function listUrlsAssoc($arUrls, $basePath, $target = "")
	{
		$result = "";
		foreach($arUrls as $url => $anchor){
			$result .= "<li class=\"list-group-item\">".self::link($basePath.$url, $anchor, "", $target)."</li>";
		}
		return "<ul class=\"list-group\">$result</ul>";
	}

	public static function first($array)
	{
		return array_slice($array, 0, 1);
	}

}