<?
class YouSubtitlesComSubtitleDownloadModel implements SubtitleDownloadInterface
{
	const SITE_DOMAIN = "yousubtitles.com";
	const SITE_URL = "https://www.yousubtitles.com";

	public function downloadSubtitle($videoId)
	{
		$requestUrl = self::SITE_URL."/loadvideo/ch--".$videoId;
		$response = CurlModel::getContent($requestUrl, "post");

		if(empty($response->links)){
			throw new Exception("На сайте ".self::SITE_DOMAIN." не найдены ссылки на скачивание субтитров");
		}

		$subtitleDownloadUrl = $this->getSubtitleDownloadUrl($response->links);

		if(!$subtitleDownloadUrl){
			throw new Exception("На сайте ".self::SITE_DOMAIN." не найдена ссылка на скачивание русских субтитров");
		}

		$subtitle = CurlModel::getContent($subtitleDownloadUrl);

		return $subtitle;
	}

	private function getSubtitleDownloadUrl($response)
	{
		$html = new simple_html_dom();
		$html->load($response);
		foreach ($html->find("p") as $paragraph){
			if(strpos($paragraph->plaintext, "Russian") !== false){
				$html->load($paragraph->outertext);
				$link = $html->find("a", 0);
				if($link){
					return self::SITE_URL.$link->href;
				}
			}
		}

		return false;
	}


}