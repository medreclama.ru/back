<?
class YoutubeApiSubtitleInfoModel extends YoutubeApi
{
	public function guardIsVideoHasManualRussianSubtitle($videoId)
	{
		$requestUrl = $this->getRequestUrl($videoId);
		$response = CurlModel::getContent($requestUrl);

		if(
			empty($response->items) ||
			!$this->isVideoHasManualRussianSubtitle($response->items)
		){
			throw new Exception("Видео не содержит русских ручных субтитров");
		}
	}

	private function getRequestUrl($videoId)
	{
		$requestUrl = self::API_BASE_URL."/captions?";
		$requestUrl .= "videoId=".$videoId."&";
		$requestUrl .= "part=snippet&";
		$requestUrl .= "key=".self::API_KEY;

		return $requestUrl;
	}

	private function isVideoHasManualRussianSubtitle($responseItems)
	{
		foreach ($responseItems as $item) {
			if($item->snippet->language == "ru" && $item->snippet->trackKind == "standard"){
				return true;
			}
		}

		return false;
	}

}
