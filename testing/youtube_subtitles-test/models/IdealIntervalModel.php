<?
class IdealIntervalModel
{
	const IDEAL_CHARS_COUNT_FOR_PARAGRAPH = 400;
	const MAX_STEPS = 300;

	public function getIdealIntervalByMedian($subtitle)
	{
		$arrMedians = $this->getAllMediansForAllIntervals($subtitle, self::MAX_STEPS);

		//Сортируем массив медиан и соотв. интервалов (asc)
		usort($arrMedians, function($a, $b){
			if ($a['deltaToIdeal'] == $b['deltaToIdeal']) {
				return 0;
			}
			return ($a['deltaToIdeal'] > $b['deltaToIdeal']) ? 1 : -1;
		});

		// Первый элемент массива (самое мин. расхождение с идеальным параграфом (idealCharsCountForParagraph))
		return $arrMedians[0]["interval"];
	}

	private function getAllMediansForAllIntervals($subtitle, $maxSteps)
	{
		$arrMedians = [];

		for($interval = 1; $interval <= $maxSteps; $interval++){
			$median = $this->getMedianCharsCountForInterval($subtitle, $interval);
			$arrMedians[] = [
				"interval" => $interval,
				"median" => $median,
				"deltaToIdeal" => abs(self::IDEAL_CHARS_COUNT_FOR_PARAGRAPH - $median)
			];
		}

		return $arrMedians;
	}

	private function getMedianCharsCountForInterval($subtitle, $interval)
	{
		$groupedSubtitle = TimeModel::groupByTimeSubtitleText($subtitle, $interval, true);
		$charsCountGrouped = [];

		foreach($groupedSubtitle as $subtitleItem){
			$charsCountGrouped[] = mb_strlen($subtitleItem, "UTF-8");
		}

		$median = $this->calculateMedian($charsCountGrouped);

		return $median;
	}

	private function calculateMedian($aValues)
	{
		$aToCareAbout = array();
		foreach ($aValues as $mValue) {
			if ($mValue >= 0) {
				$aToCareAbout[] = $mValue;
			}
		}
		$iCount = count($aToCareAbout);
		sort($aToCareAbout, SORT_NUMERIC);
		if ($iCount > 2) {
			if ($iCount % 2 == 0) {
				$f1 = floor($iCount / 2) - 1;
				$f2 = floor($iCount / 2);
				return ($aToCareAbout[$f1] + $aToCareAbout[$f2]) / 2;
			} else {
				return $aToCareAbout[$iCount / 2];
			}
		} elseif (isset($aToCareAbout[0])) {
			return $aToCareAbout[0];
		} else {
			return 0;
		}

	}

}