<?
class SubtitleModel
{
	public function getProcessedSubtitle($subtitle, IdealIntervalModel $idealIntervalModel, $minChars)
	{
		$subtitleWTimeStamps = $this->getSubtitleWTimeStamps($subtitle);
		if(!$subtitleWTimeStamps){
			throw new Exception("не удалось добавить временные метки");
		}

		// Удалить повторы строк
		$subtitleWTimeStamps = $this->uniqueMultidimArray($subtitleWTimeStamps, "lines", false);

		$idealInterval = $idealIntervalModel->getIdealIntervalByMedian($subtitleWTimeStamps);

		$subtitle = TimeModel::groupByTimeSubtitleText($subtitleWTimeStamps, $idealInterval, true);

		$subtitle = StringsModel::trimHangingLines($subtitle);
		$subtitle = StringsModel::splitBigParagraphs($subtitle, IdealIntervalModel::IDEAL_CHARS_COUNT_FOR_PARAGRAPH);
		$subtitle = StringsModel::dotAndUCForParagraphs($subtitle);

		$subtitle = "<p>".implode("</p><p>", $subtitle)."</p>";

		if($minChars && mb_strlen($subtitle, "UTF-8") < $minChars){
			throw new Exception("Субтитры короче минимальной длины");
		}

		return $subtitle;
	}

	private function getSubtitleWTimeStamps($content)
	{
		$content = $this->getSubtitleLines($content);

		$timeModel = new TimeModel;
		$content = $timeModel->addTimeToSubtitleLines($content);
		$content = $timeModel->addTimeDiffToSubtitleLines($content);
		$content = StringsModel::addTrailingDotToSubtitleLines($content);
		$content = StringsModel::upperCaseCharAfterDot($content);

		if(!$content){
			return false;
		}

		return $content;
	}


	private function getSubtitleLines($content)
	{
		$result = [];
		$lines = preg_split('/\\r\\n?|\\n/', $content);
		foreach($lines as $line){
			if(!$line) {continue;}
			if(preg_match('/^\+?\d+$/', $line)){continue;}
			$result[] = strip_tags($line);
		}

		return $result;
	}

	private function uniqueMultidimArray($array, $key, $preserveKeys = true)
	{
		$temp_array = [];
		$key_array = [];

		$i = 0;

		foreach($array as $val) {
			if (!in_array($val[$key], $key_array)) {
				$key_array[$i] = $val[$key];
				if($preserveKeys){
					$temp_array[$i] = $val;
				}else{
					$temp_array[] = $val;
				}
			}
			$i++;
		}

		 return $temp_array;
	}

}

