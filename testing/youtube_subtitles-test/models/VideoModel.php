<?
class VideoModel
{
	const STATUS_BAD = 1;
	const STATUS_PENDED = 2;
	const STATUS_IN_WORK = 3;

	public $id;
	public $title;
	public $description;
	public $thumbnail;
	public $thumbnails;
	public $dbStatusId;
	public $subtitle;
	public $message;

	public static function guardIsVideoSkipped($statusId)
	{
		if($statusId == self::STATUS_BAD){
			throw new Exception('Видео уже есть в БД со статусом "плохое"');
		}
		if($statusId == self::STATUS_IN_WORK){
			throw new Exception('Видео уже есть в БД со статусом "в работе"');
		}
	}

	public function getThumbnailUrl($videoThumbnails, $paramThumbnails)
	{
		return $videoThumbnails->$paramThumbnails->url;
	}

	public static function isStatusPended($statusId)
	{
		return $statusId == self::STATUS_PENDED;
	}

}