<?
class StringsModel
{
	// Ставим точку в реплике если след. реплика с Заглавной
	public static function addTrailingDotToSubtitleLines($subtitles)
	{
		foreach($subtitles as $k => $subtitle){
			$curLine = $subtitle["lines"];
			$nextKey = $k + 1;

			if(!empty($subtitles[$nextKey]["lines"])){
				$nextLine = $subtitles[$nextKey]["lines"];

				$curLastChar = mb_substr($curLine, (mb_strlen($curLine, "UTF-8") - 1), 1, "UTF-8");
				$nextFirstChar = mb_substr($nextLine, 0, 1, "UTF-8");

				// если последняя буква тек. строки буква и строчная (т.е. не точка)
				// а первая буква след. строки Прописная - то в тек. строку ставим точку
				if(
					preg_match('/[a-zа-я]/u', $curLastChar) &&
					preg_match('/[A-ZА-Я]/u', $nextFirstChar)
				){
					$subtitles[$k]["lines"] .= ".";
				}
			}

		}

		return $subtitles;
	}

	// Реплику с Заглавной если пред. реплика закончилась точкой (.!?)
	public static function upperCaseCharAfterDot($subtitles)
	{
		foreach($subtitles as $k => $subtitle){
			$curLine = $subtitle["lines"];
			$prevKey = $k - 1;

			if(!empty($subtitles[$prevKey]["lines"])){
				$prevLine = $subtitles[$prevKey]["lines"];

				$curFirstChar = mb_substr($curLine, 0, 1, "UTF-8");
				$prevLastChar = mb_substr($prevLine, (mb_strlen($prevLine, "UTF-8") - 1), 1, "UTF-8");

				if(
					preg_match('/[a-zа-я]/u', $curFirstChar) &&
					preg_match('/[\.\!\?]/u', $prevLastChar)
				){
					$subtitles[$k]["lines"] = self::mb_ucfirst($curLine);
				}
			}

		}

		return $subtitles;
	}

	public static function dotAndUCForParagraphs($subtitles)
	{
		foreach($subtitles as $k => $subtitle){
			$subtitles[$k] = self::mb_ucfirst($subtitle);
		}

		foreach($subtitles as $k => $subtitle){
			$lastChar = mb_substr($subtitle, (mb_strlen($subtitle, "UTF-8") - 1), 1, "UTF-8");
			if(preg_match('/[a-zа-я]/u', $lastChar)){
				$subtitles[$k] .= ".";
			}
		}

		return $subtitles;
	}

	public static function trimHangingLines($groupedSubtitles)
	{
		foreach($groupedSubtitles as $k => $paragraph){

			if(!$k){continue;}
			$prevParIndex = $k - 1;

			$firstChar = mb_substr($paragraph, 0, 1, "UTF-8");

			// Если первый символ в абзаце с малой буквы
			if (preg_match("/[a-zа-я]/u", $firstChar))
			{

				// Перебор букв в абзаце до нахождения первой заглавной
				$found = false;
				for($i = 0; $i <= mb_strlen($paragraph, "UTF-8"); $i++){
					$curChar = mb_substr($paragraph, $i, 1, "UTF-8");
					if (preg_match("/[A-ZА-Я]/u", $curChar)){
						$partStrBeforeCapChar = mb_substr($paragraph, 0, $i, "UTF-8");
						$groupedSubtitles[$k] = str_replace($partStrBeforeCapChar, "", $paragraph);
						$groupedSubtitles[$prevParIndex] = $groupedSubtitles[$prevParIndex]." ".$partStrBeforeCapChar;
						$found = true;
						break;
					}
				}
				if(!$found){
					$groupedSubtitles[$prevParIndex] = $groupedSubtitles[$prevParIndex]." ".$paragraph;
					$groupedSubtitles[$k] = "";
				}

			}

		}

		// Ставим точку в конце абзаца если нет и последний символ буква
		foreach($groupedSubtitles as $k => $subtitle){
			$groupedSubtitles[$k] = trim($subtitle);
			$subtitle = $groupedSubtitles[$k];
			$lastChar = mb_substr($subtitle, (mb_strlen($subtitle, "UTF-8") - 1), 1, "UTF-8");
			if (preg_match("/[a-zа-яA-ZА-Я]/u", $lastChar)){
				$groupedSubtitles[$k] = $subtitle.".";
			}
		}

		return $groupedSubtitles;
	}

	public static function splitBigParagraphs($paragraphs, $idealCharsCountForParagraph)
	{
		$result = [];
		foreach($paragraphs as $paragraph){

			// Если длина параграфа больше idealCharsCountForParagraph
			if(mb_strlen($paragraph, "UTF-8") > $idealCharsCountForParagraph){

				// Ищем окончания предложений (.!?)
				// PREG_OFFSET_CAPTURE - позиция в параграфе знака припенания в конце предложения ($match[1])
				preg_match_all('/[\.\!\?]/u', $paragraph, $matches, PREG_OFFSET_CAPTURE);

				$matches = $matches[0];
				$chunk = [];
				foreach($matches as $k => $match){
					if(!$k){
						$p = substr($paragraph, 0, $match[1] + 1);
					}else{
						$prevKey = $k - 1;
						$sentenceLen = $match[1] - $matches[$prevKey][1];
						$p = substr($paragraph, ($matches[$prevKey][1] + 1), $sentenceLen);
					}

					$p = trim($p);
					$chunk[] = $p;
					$chunkStr = implode(" ", $chunk);

					if(mb_strlen($chunkStr, "UTF-8") >= $idealCharsCountForParagraph){
						$result[] = $chunkStr;
						$chunk = [];
						$chunkStr = "";
					}
				}

				if(!empty($chunkStr)){
				   $result[] = $chunkStr; // последний хвост
				}


			}else{
				$result[] = $paragraph;
			}
		}

		return $result;
	}

	private static function mb_ucfirst($string, $enc = 'UTF-8')
	{
		return mb_strtoupper(mb_substr($string, 0, 1, $enc), $enc).mb_substr($string, 1, mb_strlen($string, $enc), $enc);
	}

}