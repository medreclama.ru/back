<?php
class TimeModel
{
	public function addTimeToSubtitleLines($subtitleLines)
	{
		$timeLines = [];
		$time = false;
		foreach ($subtitleLines as $subtitleLine) {
			if(strpos($subtitleLine, "-->") !== false && strpos($subtitleLine, ":") !== false){
				$time = $subtitleLine;
			}else{
				$timeLines[$time][] = $subtitleLine;
			}
		}

		if(!$timeLines){
			return false;
		}

		$result = [];
		foreach ($timeLines as $timeMark => $subtitleLines) {
			$subtitleLines = str_replace("--", "&mdash;", $subtitleLines);
			$subtitleLines = preg_replace('/\d\d:.*\d\d/', "", $subtitleLines);
			$subtitleLines = array_unique($subtitleLines);

			$timeMarks = $this->getTimeByTimemark($timeMark);

			$result[] = [
				"begin" => $timeMarks["begin"],
				"end" => $timeMarks["end"],
				"lines" => implode(" ", $subtitleLines)
			];

		}

		return $result;
	}

	public function addTimeDiffToSubtitleLines($subtitleLines)
	{
		for($i = 1; $i < count($subtitleLines); $i++){
			$curLine = $subtitleLines[$i];
			$prevLineIndex = $i - 1;
			$prevLine = $subtitleLines[$prevLineIndex];
			$subtitleLines[$i]["diff"] = $curLine["begin"] - $prevLine["end"];
		}

		return $subtitleLines;
	}


	private function getTimeByTimemark($timeMark)
	{
		$timeMark = str_replace(" --> ", ":", $timeMark);
		$timeMark = explode(":", $timeMark);

		$beginHour = $timeMark[0];
		$beginMinute = $timeMark[1];
		$beginSecond = $timeMark[2];
		$begin = ($beginHour*3600) + ($beginMinute*60) + (int)$beginSecond;

		$endHour = $timeMark[3];
		$endMinute = $timeMark[4];
		$endSecond = $timeMark[5];
		$end = ($endHour*3600) + ($endMinute*60) + (int)$endSecond;

		return [
			"begin" => $begin,
			"end" => $end
		];
	}

	public static function groupByTimeSubtitleText($subtitleLines, $interval, $returnArray = false)
	{
		$interval = (int)$interval;

		$paragraphs = [];
		$paragraph = "";
		foreach ($subtitleLines as $key => $subtitleLine)
		{
			if(!$key){
				$paragraph = $subtitleLine["lines"];
				continue;
			}

			if($interval){
				$prevKey = $key - 1;
				$diff = $subtitleLine["begin"] - $subtitleLines[$prevKey]["end"];

				if($diff >= $interval){
					$paragraphs[] = $paragraph;
					$paragraph = $subtitleLine["lines"];
				}else{
					$paragraph .= " ".$subtitleLine["lines"];
				}

			}else{
				$paragraph .= " ".$subtitleLine["lines"];
			}

		}

		if(!empty($paragraph)){
			$paragraphs[] = $paragraph;
		}

		if(empty($paragraphs)){
			throw new Exception("Не удалось сгруппировать субтитры в абзацы по времени");
		}

		if($returnArray){
			$result = $paragraphs;
		}else{
			$result = "<p>".implode("</p><p>", $paragraphs)."</p>";
		}

		return $result;
	}

}