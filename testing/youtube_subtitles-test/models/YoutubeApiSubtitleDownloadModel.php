<?
class YoutubeApiSubtitleDownloadModel extends YoutubeApi implements SubtitleDownloadInterface
{
	public function downloadSubtitle($videoId)
	{
		$requestUrl = $this->getRequestUrl($videoId);
		$response = CurlModel::getContent($requestUrl);

		return $response;
	}


	private function getRequestUrl($videoId)
	{
		return self::API_BASE_URL."/captions/".$videoId."?key=".self::API_KEY;
	}

}