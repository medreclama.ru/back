<?
class DbModel
{
	const SUBTITLES_TABLE = "youtube_subtitles";

	public static function getVideoDbStatus($videoId)
	{
		$videoId = mysql_real_escape_string($videoId);
		$sqlQuery = "SELECT id, status FROM ".self::SUBTITLES_TABLE." WHERE video_id = '".$videoId."'";
		$sqlRes = mysql_query($sqlQuery);

		if(!mysql_num_rows($sqlRes)) {
			return false;
		}

		$result = mysql_fetch_array($sqlRes, MYSQL_ASSOC);

		return $result["status"];
	}

	public function save($postData)
	{
		if(!$postData){
			return json_encode([
				"status" => false,
				"message" => "Ошибка при сохранении видео (поиск в сессии)"
			]);
		}

		$data = [
			"video_id" => !empty($postData["id"]) ? $postData["id"] : "",
			"status" => !empty($postData["status"]) ? $postData["status"] : "",
			"title" => !empty($postData["title"]) ? htmlspecialchars_decode($postData["title"]) : "",
			"description" => !empty($postData["description"]) ? htmlspecialchars_decode($postData["description"]) : "",
			"search_request" => !empty($postData["search_request"]) ? $postData["search_request"] : "",
			"subtitle" => !empty($postData["subtitle"]) ? htmlspecialchars_decode($postData["subtitle"]) : "",
		];


		$columns = implode(", ", array_keys($data));

		$values  = array_map(function($value){
			$value = mysql_real_escape_string($value);
			return '"'.$value.'"';
		}, $data);
		$values  = implode(", ", $values);

		$sqlQuery = "INSERT INTO ".self::SUBTITLES_TABLE." (".$columns.") VALUES (".$values.")";

		if(!mysql_query($sqlQuery)){
			return json_encode([
				"status" => false,
				"message" => "Ошибка при сохранении видео (запрос к БД)"
			]);
		}

		return json_encode([
			"status" => true,
			"message" => "Видео успешно сохранено"
		]);
	}

}