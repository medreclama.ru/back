<?
class YoutubeApiVideoSearchModel extends YoutubeApi
{
	public function getSearchResults($searchRequest, $pageToken, $videosPerPage)
	{
		$requestUrl = $this->getRequestUrl($searchRequest, $pageToken, $videosPerPage);
		$response = CurlModel::getContent($requestUrl);

		if(empty($response->items)){
			return false;
		}

		$result = [];
		foreach ($response->items as $item)
		{
			$video = new VideoModel;
			$video->id = $item->id->videoId;
			$video->title = $item->snippet->title;
			$video->description = $item->snippet->description;
			$video->thumbnails = $item->snippet->thumbnails;

			$result["videos"][] = $video;
		}

		if(!empty($response->prevPageToken)){
			$result["prevPageToken"] = $response->prevPageToken;
		}

		if(!empty($response->nextPageToken)){
			$result["nextPageToken"] = $response->nextPageToken;
		}

		return $result;
	}

	private function getRequestUrl($searchRequest, $pageToken, $videosPerPage)
	{
		$requestUrl = self::API_BASE_URL."/search?";

		$urlParams = [];

		$urlParams[] = "q=".urlencode($searchRequest);
		$urlParams[] = "key=".self::API_KEY;
		$urlParams[] = "part=snippet";
		$urlParams[] = "type=video";
		$urlParams[] = "videoCaption=closedCaption";

		if($pageToken){
			$urlParams[] = "pageToken=".$pageToken;
		}

		if($videosPerPage){
			$urlParams[] = "maxResults=".$videosPerPage;
		}

		$urlParams = implode("&", $urlParams);
		$requestUrl .= $urlParams;

		return $requestUrl;
	}

}