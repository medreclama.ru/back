<?
require $_SERVER["DOCUMENT_ROOT"]."/_env.php";
require $_SERVER["DOCUMENT_ROOT"]."/_conf/main.cfg";
require $_SERVER["DOCUMENT_ROOT"]."/_service/mysql.php";
require __DIR__."/require_models.php";

$videos = false;
$prevPageToken = false;
$nextPageToken = false;
$thumbnails = "default";
$error = false;

$youtubeApiVideoSearchModel = new YoutubeApiVideoSearchModel;
$youtubeApiSubtitleInfoModel = new YoutubeApiSubtitleInfoModel;
$subtitleModel = new SubtitleModel;
$idealIntervalModel = new IdealIntervalModel;
$subtitleDownloadModel = new YouSubtitlesComSubtitleDownloadModel();

if(!empty($_POST["get_videos"]) OR !empty($_POST["get_videos_prev"]) OR !empty($_POST["get_videos_next"])){
	if(!empty($_POST["search_request"])){

		$searchRequest = $_POST["search_request"];
		$videosPerPage = !empty($_POST["videos_per_page"]) ? $_POST["videos_per_page"] : false;

        $pageToken = !empty($_POST["get_videos_prev"]) ? $_POST["page_token_prev"] : false;
        $pageToken = !empty($_POST["get_videos_next"]) ? $_POST["page_token_next"] : false;

		$minChars = !empty($_POST["min_chars"]) ? $_POST["min_chars"] : false;
		$thumbnails = !empty($_POST["thumbnails"]) ? $_POST["thumbnails"] : "default";

		try {
			$searchResults = $youtubeApiVideoSearchModel->getSearchResults($searchRequest, $pageToken, $videosPerPage);
		}catch (Exception $e){
			$error = $e->getMessage();
		}

		if(!$error){
			if(!$searchResults){
				$error = "Нет найденных видео по вашему запросу";
			} else {
				$videos = $searchResults["videos"];

				/** @var $video VideoModel */
				foreach ($videos as $key => $video){
					try{
						$video->dbStatusId = DbModel::getVideoDbStatus($video->id);

						VideoModel::guardIsVideoSkipped($video->dbStatusId);
						$youtubeApiSubtitleInfoModel->guardIsVideoHasManualRussianSubtitle($video->id);

						$video->thumbnail = $video->getThumbnailUrl($video->thumbnails, $thumbnails);
						$subtitle = $subtitleDownloadModel->downloadSubtitle($video->id);
						$video->subtitle = $subtitleModel->getProcessedSubtitle($subtitle, $idealIntervalModel, $minChars);
					}catch (Exception $e) {
						$video->message = $e->getMessage();
					}

					$videos[$key] = $video;
				}

				$prevPageToken = !empty($searchResults["prevPageToken"]) ? $searchResults["prevPageToken"] : false;
				$nextPageToken = !empty($searchResults["nextPageToken"]) ? $searchResults["nextPageToken"] : false;
			}
		}

	}
}

if(!empty($_GET["saveToDb"])){
	$dbModel = new DbModel;
	exit($dbModel->save($_POST));
}