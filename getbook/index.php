<?php
require($_SERVER['DOCUMENT_ROOT'].'/includes/phpmailer/class.phpmailer.php');

date_default_timezone_set('Europe/Moscow');
$status = false;
if(
        !empty($_POST['name'])
    AND !empty($_POST['email'])
){
    $name      = $_POST['name'];
    $emailUser = $_POST['email'];

    $body = file_get_contents(__DIR__.'/thank_you_email.htm');

    $email = new PHPMailer();
    $email->CharSet = "UTF-8";
    $email->From = "reklama@krasiviemedia.ru";
    $email->FromName = 'Агентство медицинского маркетинга "Medreclama"';
    $email->Subject = $name.", наконец-то ваша книга у вас!";
    $email->Body = $body;
    $email->Sender = $email->From;
    $email->AddAddress($emailUser);
    $email->IsHTML(true); // send as HTML

    $result = $email->Send();

    setcookie('sendBook_name',  '', time() - 3600, '/');
    setcookie('sendBook_email', '', time() - 3600, '/');

    if($result){
        $status = true;
    }
}

exit(json_encode($status));







